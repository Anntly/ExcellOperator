# 导入

导入采用的是单线程采用SAX模式进行数据的读取，存放数据到`BlockQueue`中，建议使用无界队列。多线程获取数据批量进行提交操作。在案例中可以查看具体使用。注意：无法保证多个线程之间的事务，也无法保证数据提交的顺序性

```java
// 保存数据的阻塞队列
BlockingQueue blockingQueue = new LinkedBlockingQueue<>();
        ExcelOperator excelOperator = new ExcelOperator.ExcelUtilBuilder<Student>(file, blockingQueue, Student.class)
                .setSkipFirstRow(true)
                .build();
        // 参数.比如创建用户等，可以直接传入，由于使用到了多线程传入map的切记process方法中不要修改数据避免安全问题的产生
        Map<String, Object> param = new HashMap<>();
        param.put("create_name", "小刚");
        param.put("create_Time", new Date());
        // batcheNum是每1000条执行一次process方法，由于使用的多线程读取阻塞队列，顺序不能保证
        excelOperator.paresExcelSingleThread(1000, param, new Process<List, Map>() {
            @Override
            public void process(List list, Map map) {
                // 上传数据,MybatisPlus后面指定的batchSize是该插件每1000条提交一次，否则默认是30条一次
                studentService.saveBatch(list,1000);
                // map参数可以根据自己要求对数据再进行处理
            }
        });
```

`Process`即为提供的处理数据的接口，list为解析出的数据，最多为设定的batchSize，param为参数(只读)