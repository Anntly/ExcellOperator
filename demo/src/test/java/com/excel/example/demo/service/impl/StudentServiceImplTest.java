package com.excel.example.demo.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.excel.example.demo.entity.Student;
import com.excel.example.demo.service.StudentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class StudentServiceImplTest {

    @Autowired
    private StudentServiceImpl studentService;

    @Test
    public void testQueryAll(){
        List<Student> students = studentService.queryAll();
        students.stream().forEach(System.out::println);
    }

    @Test
    public void batchInsert() throws CloneNotSupportedException {
        Student student = new Student();
        student.setName("小红");
        //student.setBirthday(new Date());
        student.setCity("成都");
        student.setCountry("中国");
        student.setProvince("四川");
        student.setSex("男");
        List<Student> list = new ArrayList<>();
        for (int i = 1; i < 10000; i++) {
            Student temp = (Student) student.clone();
            //student.setId(i);
            student.setName("小红" + i);
            list.add(temp);
        }
        studentService.saveBatch(list,1000);
    }

    @Test
    public void selectPage(){
        IPage<Student> iPage = new Page<>(1,3000);
//        QueryWrapper<Student> wrapper = new QueryWrapper<>();
//        Student student = new Student();
        IPage<Student> page = studentService.page(iPage);
        List<Student> records = page.getRecords();
        System.out.println(records.get(0));
        System.out.println(records.get(records.size() - 1));
        //records.stream().forEach(System.out::println);
    }

    @Test
    public void testStream() throws CloneNotSupportedException {
        Student student = new Student();
        student.setName("小红");
        student.setBirthday(new Date());
        student.setCity("成都");
        student.setCountry("中国");
        student.setProvince("四川");
        student.setSex("男");

        //list.stream().
    }


}