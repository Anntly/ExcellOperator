package com.excel.example.demo.enums;

public enum ExcelVersion {
    // 2003版本最多65536行数据，建议不要超过5w，建议按照默认值即可
    V2003(".xls",3000,100),
    // 2007版本最多1048576行数据，建议最多不要超过10w
    V2007(".xlsx",3000,100),
    ;

    // 后缀
    private String suffix;
    // 默认最大行
    private Integer maxRow;
    // 默认最大列
    private Integer maxColumn;

    ExcelVersion(String suffix, Integer maxRow, Integer maxColumn) {
        this.suffix = suffix;
        this.maxRow = maxRow;
        this.maxColumn = maxColumn;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public Integer getMaxRow() {
        return maxRow;
    }

    public void setMaxRow(Integer maxRow) {
        this.maxRow = maxRow;
    }

    public Integer getMaxColumn() {
        return maxColumn;
    }

    public void setMaxColumn(Integer maxColumn) {
        this.maxColumn = maxColumn;
    }
}
