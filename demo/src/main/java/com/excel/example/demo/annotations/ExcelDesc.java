package com.excel.example.demo.annotations;

import java.lang.annotation.*;

/**
 * 用于描述Excel的信息
 * 类上为sheet名称，超过一个sheet就会在后+1
 * 字段上为单元格标题名称
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD,ElementType.TYPE})
@Inherited
public @interface ExcelDesc {
    String value();
}
