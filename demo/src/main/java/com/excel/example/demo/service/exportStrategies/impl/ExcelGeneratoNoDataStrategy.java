package com.excel.example.demo.service.exportStrategies.impl;

import com.excel.example.demo.entity.CascadeDropdownData;
import com.excel.example.demo.entity.SelfCellStyle;
import com.excel.example.demo.enums.ExcelVersion;
import com.excel.example.demo.handlers.DropDownListHandler;
import com.excel.example.demo.service.exportStrategies.AbstractExcelGeneratorStrategy;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

/**
 * @Auther: Anntly
 * @Date: 2020/3/30 13:06
 * @Description: 没有数据的excel导出
 */
public class ExcelGeneratoNoDataStrategy extends AbstractExcelGeneratorStrategy {

    // 隐藏的列
    private List<Integer> hiddenColumns;
    // 下拉框数据,true代表按照行插入；false代表按照列插入；integer代表在多少行/列插入，String[]为数据
    private Map<Boolean, Map<Integer, String[]>> dropdownList;
    // 级联下拉框
    private List<CascadeDropdownData> cascadeDropdownDataList;

    public ExcelGeneratoNoDataStrategy(InputStream templateStream, String sheetName,
                                       Class clazz, String[] headers,
                                       ExcelVersion version, boolean needHeaderStyle,
                                       boolean needCellStyle, SelfCellStyle headStyle,
                                       SelfCellStyle cellStyle, Integer maxRow,
                                       Integer maxColumn, Map<String, DateTimeFormatter> dateFormatterPattern,
                                       Field[] declaredFields,List<Integer> hiddenColumns,
                                       Map<Boolean, Map<Integer, String[]>> dropdownList,
                                       List<CascadeDropdownData> cascadeDropdownDataList) {
        super(templateStream, sheetName, clazz, headers, version, needHeaderStyle, needCellStyle, headStyle, cellStyle, maxRow, maxColumn, dateFormatterPattern, declaredFields);
        this.hiddenColumns = hiddenColumns;
        this.dropdownList = dropdownList;
        this.cascadeDropdownDataList = cascadeDropdownDataList;
    }

    @Override
    public void generateExcel(OutputStream outputStream){
        super.checkHeaderIsRight();
        Workbook workbook = null;
        try {
            workbook = super.initWorkBook();
            super.initCellStyle(workbook);
            Sheet sheet;
            if (templateStream != null) {
                sheet = workbook.getSheetAt(0);
            } else {
                sheet = workbook.createSheet(sheetName);
            }
            // 创建标题行
            Row headerRow = sheet.createRow(0);
            super.initHeaderRow(sheet, headerRow, headStyle);
            if (hiddenColumns != null || dropdownList != null || cascadeDropdownDataList != null) {
                DropDownListHandler dropDownListHandler = new DropDownListHandler(1, 0, this.dropdownList);
                super.handleOrhersMuliteFiles(maxRow, dropDownListHandler, cascadeDropdownDataList, hiddenColumns, workbook, sheet);
            }
            workbook.write(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            close(workbook);
        }
    }

    @Override
    public void generateExcelMultipleFile(ZipOutputStream zop) throws Exception {
        throw new UnsupportedOperationException("不支持多文件导出");
    }
}