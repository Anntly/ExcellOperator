package com.excel.example.demo.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.excel.example.demo.annotations.DateFormat;
import com.excel.example.demo.annotations.DropdownList;
import com.excel.example.demo.annotations.ExcelDesc;
import com.excel.example.demo.entity.CascadeDropdownData;
import com.excel.example.demo.entity.SelfCellStyle;
import com.excel.example.demo.enums.DropdownListType;
import com.excel.example.demo.enums.ExcelVersion;
import com.excel.example.demo.enums.ExportExcelType;
import com.excel.example.demo.handlers.*;
import com.excel.example.demo.service.DataPartition;
import com.excel.example.demo.service.Process;
import com.excel.example.demo.service.exportStrategies.AbstractExcelGeneratorStrategy;
import com.excel.example.demo.service.exportStrategies.impl.*;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.LongAdder;
import java.util.zip.ZipOutputStream;

/*
 * @Author Anntly
 * @Description Excel工具类
 * @Date 2020/3/19 15:20
 **/
public class ExcelOperator<T> {

    // 版本
    private ExcelVersion version;
    // sheet名称
    private volatile String sheetName;
    // 传入的模板excel
    private volatile InputStream templateStream;
    // 用于读取的Excel文件
    private MultipartFile excelFile;
    // 用于存储读取的Excel数据的阻塞队列
    private BlockingQueue<T> blockingQueue;
    // 本机的CPU核心数量
    static final int nThreads = Runtime.getRuntime().availableProcessors();
    // 用于判断excel文件是否读取完毕,当state为0代表已经读取结束
    private AtomicInteger finishState = new AtomicInteger();
    // 类类型
    private Class clazz;
    // 是否需要标题单元格样式
    private boolean needHeaderStyle;
    // 是否需要单元格样式(数据量比较大的时候添加单元格样式会消耗部分时间和内存,初始化时，当数据量大于10w的时候会取消样式)
    private boolean needCellStyle;
    // 自定义标题行样式
    private SelfCellStyle headStyle = new SelfCellStyle();
    // 普通行样式
    private SelfCellStyle cellStyle = new SelfCellStyle();
    // 标题
    private String[] headers;
    // 数据
    private List dataList;
    // 是否跳过第一行
    private boolean isSkipFirstRow;
    // 设置最大行数
    private volatile Integer maxRow;
    // 设置最大列
    private volatile Integer maxColumn;
    // 隐藏的列
    private volatile List<Integer> hiddenColumns;
    // 只读列下标
    private volatile List<Integer> onlyReadColumns;
    // 下拉框数据,true代表按照行插入；false代表按照列插入；integer代表在多少行/列插入，String[]为数据
    Map<Boolean, Map<Integer, String[]>> dropdownList;
    // 级联下拉框数据
    /*
        注意：级联下拉需要创建名称管理器，名称管理器是不支持特殊符号的，
     *  下拉列表的数据最好是不要使用他特殊符号(已踩坑)，比如 括号、冒号等
     *  如果需要特殊符号建议在模板中自己创建名称管理器来达到效果
     */
    private List<CascadeDropdownData> cascadeDropdownDataList;
    // key:字段名 value:格式化
    private Map<String, DateTimeFormatter> dateFormatterPattern;
    // 保存实体类的字段属性
    private Field[] declaredFields;
    // 导出时数据分区接口
    private DataPartition dataPartition;
    // 分区时查询数据时用的参数
    private Map partitionParam;

    /**
     * @return void
     * @Author Anntly
     * @Description 统一导出excel的入口
     * @Date 2020/3/28 17:24
     * @Param [fileName, response, type]
     **/
    public void downloadExcel(String fileName, HttpServletResponse response, ExportExcelType type) throws Exception {
        if((dataList == null || dataList.isEmpty())
            && dataPartition == null){
            downloadMultipleNoData(fileName,response);
        }else {
            if (ExportExcelType.MULTIPLE_SHEETS.equals(type)) {
                downloadMultipleSheet(fileName, response);
            } else if (ExportExcelType.MULTIPLE_FILES.equals(type)) {
                downloadMultipleExcel(fileName, response, false);
            } else if (ExportExcelType.MULTIPLE_FILES_THREADS.equals(type)) {
                downloadMultipleExcel(fileName, response, true);
            }
        }
    }

    /**
     * 下载excel(单文件，多Sheet方式)
     *
     * @param fileName 文件名
     * @param response
     */
    private void downloadMultipleNoData(String fileName, HttpServletResponse response) {
        BufferedOutputStream bof = null;
        try {
            bof = StreamHandler.getBufferedOutputStream(fileName, response,version.getSuffix());
            AbstractExcelGeneratorStrategy excelGeneratorStrategy =
                    new ExcelGeneratoNoDataStrategy(templateStream,sheetName,
                            clazz,headers,version,needHeaderStyle,needCellStyle,headStyle,cellStyle,
                            maxRow,maxColumn,dateFormatterPattern,declaredFields,hiddenColumns,
                            dropdownList,cascadeDropdownDataList);
            excelGeneratorStrategy.generateExcel(bof);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(bof);
            close(templateStream);
        }
    }

    /**
     * 下载excel(单文件，多Sheet方式)
     *
     * @param fileName 文件名
     * @param response
     */
    private void downloadMultipleSheet(String fileName, HttpServletResponse response) {
        BufferedOutputStream bof = null;
        try {
            bof = StreamHandler.getBufferedOutputStream(fileName, response,version.getSuffix());
            AbstractExcelGeneratorStrategy excelGeneratorStrategy =
                    new ExcelGeneratoMultipleSheetStrategy(dataList,templateStream,sheetName,
                            clazz,headers,version,needHeaderStyle,needCellStyle,headStyle,cellStyle,
                            maxRow,maxColumn,dateFormatterPattern,declaredFields,hiddenColumns,
                            dropdownList,cascadeDropdownDataList);
            excelGeneratorStrategy.generateExcel(bof);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(bof);
            close(templateStream);
        }
    }

    /**
     * 下载Excel，多个Excel，每个excel只有一个sheet
     *
     * @param fileName
     * @param response
     * @throws Exception
     */
    private void downloadMultipleExcel(String fileName, HttpServletResponse response, boolean isThreads) throws Exception {
        ServletOutputStream out = StreamHandler.getServletOutputStream(fileName,response,".zip");
        ZipOutputStream zop = new ZipOutputStream(out);
        try {
            if(isThreads){
                if (dataPartition != null){
                    AbstractExcelGeneratorStrategy excelGeneratorStrategy =
                            new ExcelGeneratoMultipleFileThreadsPartitionStrategy(dataPartition,partitionParam,
                                    fileName,templateStream,sheetName,
                                    clazz,headers,version,needHeaderStyle,needCellStyle,headStyle,cellStyle,
                                    maxRow,maxColumn,dateFormatterPattern,declaredFields,hiddenColumns,
                                    dropdownList,cascadeDropdownDataList);
                    excelGeneratorStrategy.generateExcelMultipleFile(zop);
                }else {
                    AbstractExcelGeneratorStrategy excelGeneratorStrategy =
                            new ExcelGeneratoMultipleFileThreadsStrategy(dataList,
                                    fileName,templateStream,sheetName,
                                    clazz,headers,version,needHeaderStyle,needCellStyle,headStyle,cellStyle,
                                    maxRow,maxColumn,dateFormatterPattern,declaredFields,hiddenColumns,
                                    dropdownList,cascadeDropdownDataList);
                    excelGeneratorStrategy.generateExcelMultipleFile(zop);
                }
            }else {
                AbstractExcelGeneratorStrategy excelGeneratorStrategy =
                        new ExcelGeneratoMultipleFileStrategy(dataList,fileName,templateStream,sheetName,
                                clazz,headers,version,needHeaderStyle,needCellStyle,headStyle,cellStyle,
                                maxRow,maxColumn,dateFormatterPattern,declaredFields,hiddenColumns,
                                dropdownList,cascadeDropdownDataList);
                excelGeneratorStrategy.generateExcelMultipleFile(zop);
            }
        } finally {
            close(zop);
            close(out);
            close(templateStream);
        }
    }

    /**
     * @return void
     * @Description 生产者消费者模型，单线程读取;多线程操作数据
     * @Date 2020/3/23 17:39
     * @Param batchNum  每 batchNum进行一次 批量处理操作
     * @Param consumer  需要在读取数据后执行的操作
     * @Param param     执行操作时需要的参数
     **/
    public void paresExcelSingleThread(int batchNum, Map<String, Object> param, Process<List, Map> process) throws Exception {
        // 先判断文件类型,zip就先解压
        String fileName = excelFile.getOriginalFilename();
        Map<String, List<InputStream>> inputStreams = StreamHandler.InitStreams(excelFile, fileName);
        if (inputStreams.size() <= 0) {
            throw new RuntimeException("获取文件流失败");
        }
        // BeanUtils注册用于转换日期格式的转换器
        ConvertUtils.register(new Converter() {
            @Override
            public Object convert(Class type, Object value) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    return simpleDateFormat.parse(value.toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }, Date.class);
        int firstRow = isSkipFirstRow ? 1 : 0;
        String[] fieldNames = new String[declaredFields.length];
        for (int i = 0; i < declaredFields.length; i++) {
            fieldNames[i] = declaredFields[i].getName();
        }
        // 创建一个只有一个线程的线程池用于读取excel数据
        CyclicBarrier cyclicBarrier = new CyclicBarrier(nThreads + 1);
        ExecutorService readPool = Executors.newSingleThreadExecutor();
        ExecutorService processPool = Executors.newFixedThreadPool(nThreads);
        // 初始化finshState状态
        List<InputStream> inputStreamsXls = new ArrayList<>();
        List<InputStream> inputStreamsXlsx = new ArrayList<>();
        if (inputStreams.containsKey("xls")) {
            inputStreamsXls = inputStreams.get("xls");
            this.finishState.addAndGet(inputStreamsXls.size());
        }
        if (inputStreams.containsKey("xlsx")) {
            inputStreamsXlsx = inputStreams.get("xlsx");
            this.finishState.addAndGet(inputStreamsXlsx.size());
        }
        for (InputStream stream : inputStreamsXlsx) {
            readPool.execute(() -> {
                // 默认跳过首行
                processOneSheet(stream);
            });
        }
        for (InputStream stream : inputStreamsXls) {
            readPool.execute(() -> {
                parseSheet(stream, firstRow, fieldNames);
            });
        }
        LongAdder adder = new LongAdder();
        for (int i = 0; i < nThreads; i++) {
            processPool.execute(() -> {
                try {
                    List list = new ArrayList<>();
                    while (true) {
                        if (isFinish() && blockingQueue.isEmpty()) {
                            break;
                        }
                        int count = 0;
                        while (count < batchNum) {
                            // 防止数量不能被BatchNum整除
                            Object o = blockingQueue.poll(1, TimeUnit.MILLISECONDS);
                            if (o != null) {
                                list.add(o);
                            }
                            count++;
                        }
                        adder.add(list.size());
                        if (!list.isEmpty()) {
                            process.process(list, param);
                        }
                        list.clear();
                    }
                    cyclicBarrier.await();
                    System.out.println("提交完毕");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            });
        }
        cyclicBarrier.await();
        // 关闭线程池
        readPool.shutdown();
        processPool.shutdown();
        System.out.println("解析完毕:" + adder.intValue());
//        while(!isFinish()){
//            TimeUnit.SECONDS.sleep(1);
//        }
//        System.out.println(blockingQueue.size());
    }

    /**
     * @return void
     * @Description SAX模式读取 xlsx(该模式数据量较大)
     * @Date 2020/3/25 14:59
     * @Param [inputStream]
     **/
    private void processOneSheet(InputStream inputStream) {
        InputStream sheet2 = null;
        OPCPackage pkg = null;
        try {
            pkg = OPCPackage.open(inputStream);
            XSSFReader r = new XSSFReader(pkg);
            SharedStringsTable sst = r.getSharedStringsTable();
            XMLReader parser = new SheetHandler(sst, this.clazz, this.blockingQueue).fetchSheetParser();
            sheet2 = r.getSheet("rId1");
            InputSource sheetSource = new InputSource(sheet2);
            parser.parse(sheetSource);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 执行完毕 finishState减一
            this.finishState.decrementAndGet();
            close(pkg);
            close(sheet2);
        }
    }

    /**
     * @return void
     * @Description SAX模式处理多个Sheet
     * @Date 2020/3/25 17:42
     * @Param [inputStream]
     **/
    private void processAllSheets(InputStream inputStream) throws Exception {
        OPCPackage pkg = null;
        InputStream sheet = null;
        try {
            pkg = OPCPackage.open(inputStream);
            XSSFReader r = new XSSFReader(pkg);
            SharedStringsTable sst = r.getSharedStringsTable();
            XMLReader parser = new SheetHandler(sst, this.clazz, this.blockingQueue).fetchSheetParser();
            Iterator<InputStream> sheets = r.getSheetsData();
            while (sheets.hasNext()) {
                System.out.println("Processing new sheet:\n");
                sheet = sheets.next();
                InputSource sheetSource = new InputSource(sheet);
                parser.parse(sheetSource);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            close(pkg);
            close(sheet);
        }
    }

    /**
     * @return void
     * @Description 读取 xls 格式的数据，一般数据量较少直接读取即可
     * @Date 2020/3/20 11:53
     * @Param [sheet]
     **/
    private void parseSheet(InputStream inputStream, int firstRow, String[] fieldNames) {
        try {
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
            // 获取一共有多少个Sheet(会包含隐藏列，判断是否和字段长度相等且row的长度且不为最后一个sheet的row个数应该为maxRow)
            // 如果需要细致判断，可以从实体类中获取标题进行对照
            int numberOfSheets = workbook.getNumberOfSheets();
            for (int i = 0; i < numberOfSheets; i++) {
                HSSFSheet sheet = workbook.getSheetAt(i);
                int lastRow = sheet.getPhysicalNumberOfRows();
                // 验证标题与实体类是否符合
                if (!checkSheetTemplateIsRight(sheet, fieldNames)) {
                    continue;
                } else if (i != numberOfSheets - 1 && lastRow != maxRow) {
                    continue;
                }
                JSONObject json = new JSONObject();
                for (int rowNum = firstRow; rowNum < lastRow; rowNum++) {
                    Row row = sheet.getRow(rowNum);
                    for (int j = 0; i < fieldNames.length; j++) {
                        Cell cell = row.getCell(j);
                        json.put(fieldNames[j], ValueHandler.getCellValue(cell));
                    }
                    Object object = json.toJavaObject(clazz);
                    blockingQueue.put((T) object);
                    json.clear();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(inputStream);
            // 执行完毕 finishState减一
            this.finishState.decrementAndGet();
        }

    }

    /**
     * @return boolean
     * @Description 检测标题行是否与实体类匹配,解析时
     * @Date 2020/3/20 13:53
     * @Param [sheet, fieldNames,firstColumn]
     **/
    private boolean checkSheetTemplateIsRight(Sheet sheet, String[] fieldNames) {
        int firstRowNum = sheet.getFirstRowNum();
        Row headerRow = sheet.getRow(firstRowNum);
        return headerRow.getPhysicalNumberOfCells() == fieldNames.length;
    }

    /**
     * @Description  判断数据是否已经读取完毕
     * @Date 2020/3/29 17:17
     * @Param []
     * @return boolean
     **/
    private boolean isFinish() {
        return finishState.get() == 0;
    }

    /**
     * 合并单元格
     *
     * @param sheet
     * @param firstRow 开始行
     * @param lastRow  结束行
     * @param firstCol 开始列
     * @param lastCol  结束列
     */
    private void mergeRegion(Sheet sheet, int firstRow, int lastRow, int firstCol, int lastCol) {
        sheet.addMergedRegion(new CellRangeAddress(firstRow, lastRow, firstCol, lastCol));
    }

    private ExcelOperator(ExcelUtilBuilder builder) {
        this.clazz = builder.clazz;
        if (builder.excelFile != null) {
            // 如果是读excel数据，由于上传的文件可能是压缩文件，
            // 考虑到解压等操作，将WorkBook的处理，版本的判断放在解析中
            this.excelFile = builder.excelFile;
            this.blockingQueue = builder.blockingQueue;
            this.isSkipFirstRow = builder.isSkipFirstRow;
        } else {
            this.version = builder.version;
            this.templateStream = builder.templateStream;
            this.dataList = builder.dataList;
            this.needHeaderStyle = builder.needHeaderStyle;
            this.needCellStyle = builder.needCellStyle;
            if (null != this.dataList && this.dataList.size() >= 100000) {
                this.needCellStyle = false;
            }
            this.headStyle = builder.cellStyle;
            this.cellStyle = builder.cellStyle;
            // 初始化最大行与最大列
            InitMaxRowAndMaxColumn(builder);
            this.hiddenColumns = builder.hiddenColumns;
            this.onlyReadColumns = builder.onlyReadColumns;
            this.dropdownList = builder.dropdownList;
            this.cascadeDropdownDataList = builder.cascadeDropdownDataList;
        }
        // 初始化标题和日期格式
        InitTitleAndDateFormatterAndDropList(builder);
        this.dataPartition = builder.dataPartition;
        this.partitionParam = builder.partitionParam;
    }

    private void InitMaxRowAndMaxColumn(ExcelUtilBuilder builder) {
        if (null == this.maxRow) {
            this.maxRow = version.getMaxRow();
        } else {
            this.maxRow = builder.maxRow;
        }
        if (null == this.maxColumn) {
            if (null != maxColumn && headers.length >= maxColumn) {
                throw new IllegalArgumentException("列数超过模板的最大限制");
            }
            this.maxColumn = version.getMaxColumn();
        } else {
            this.maxColumn = builder.maxColumn;
        }
    }

    private void InitTitleAndDateFormatterAndDropList(ExcelUtilBuilder builder) {
        if (null == headers) {
            getHeaderAndAnnotations();
        } else {
            this.headers = builder.headers;
        }
    }

    // 通过注解获取sheetName名称与标题 下拉框等
    private void getHeaderAndAnnotations() {
        // 获取sheetName
        boolean isSheetNameDeclare = clazz.isAnnotationPresent(ExcelDesc.class);
        if (isSheetNameDeclare) {
            ExcelDesc sheetDesc = (ExcelDesc) clazz.getAnnotation(ExcelDesc.class);
            this.sheetName = sheetDesc.value();
        }
        // 获取字段
        this.declaredFields = clazz.getDeclaredFields();
        List<String> headerNames = new ArrayList<>(declaredFields.length);
        dateFormatterPattern = new HashMap<>();
        Map<Boolean, Map<Integer, String[]>> tempDropdownList = new HashMap<>();
        List<DropdownList> cascadeDropDownList = new ArrayList<>();
        Map<String, DropdownList> topList = new HashMap<>();
        List<CascadeDropdownData> tempCascadeDropdownDataList = new ArrayList<>();
        for (Field declaredField : declaredFields) {
            boolean isHeaderDeclare = declaredField.isAnnotationPresent(ExcelDesc.class);
            boolean isFormatterDeclare = declaredField.isAnnotationPresent(DateFormat.class) || declaredField.getType() == Date.class;
            boolean isDropListDeclare = declaredField.isAnnotationPresent(DropdownList.class);
            // 如果定义了标题名称直接使用
            if (isHeaderDeclare) {
                headerNames.add(declaredField.getAnnotation(ExcelDesc.class).value());
            } else {
                // 否则使用字段名称
                headerNames.add(declaredField.getName());
            }
            // 添加日期转换器
            if (isFormatterDeclare) {
                dateFormatterPattern.put(declaredField.getName(), DateTimeFormatter.ofPattern(declaredField.getAnnotation(DateFormat.class).value() == null ? "yyyy-MM-dd HH:mm:ss" : declaredField.getAnnotation(DateFormat.class).value()));
            }
            if (isDropListDeclare) {
                // 判断是普通下拉框还是级联下拉框
                DropdownList dropdownList = declaredField.getAnnotation(DropdownList.class);
                if (dropdownList.parentIndex() == -1 && !dropdownList.isTopParent()) { // 普通下拉框
                    if (!"".equals(dropdownList.valueJson())) {
                        List<String> dropList = JSONArray.parseArray(dropdownList.valueJson(), String.class);
                        String[] dropArr = new String[dropList.size()];
                        dropList.toArray(dropArr);
                        Map<Integer, String[]> dropMap = new HashMap<>();
                        dropMap.put(dropdownList.index(), dropArr);
                        if (dropdownList.type().equals(DropdownListType.BY_COLUMN)) {
                            tempDropdownList.put(Boolean.FALSE, dropMap);
                        } else {
                            tempDropdownList.put(Boolean.TRUE, dropMap);
                        }
                    }
                } else {
                    if (dropdownList.isTopParent()) {
                        topList.put(declaredField.getName(), dropdownList);
                    } else {
                        cascadeDropDownList.add(dropdownList);
                    }
                }
            }
        }
        // 处理下拉框
        if (!tempDropdownList.isEmpty()) {
            if (null == this.dropdownList) {
                this.dropdownList = new HashMap<>();
            }
            //this.dropdownList
            if (tempDropdownList.containsKey(Boolean.FALSE)) {
                if (this.dropdownList.containsKey(Boolean.FALSE)) {
                    this.dropdownList.get(Boolean.FALSE).putAll(tempDropdownList.get(Boolean.FALSE));
                } else {
                    this.dropdownList.put(Boolean.FALSE, tempDropdownList.get(Boolean.FALSE));
                }
            } else if (tempDropdownList.containsKey(Boolean.TRUE)) {
                if (this.dropdownList.containsKey(Boolean.TRUE)) {
                    this.dropdownList.get(Boolean.TRUE).putAll(tempDropdownList.get(Boolean.TRUE));
                } else {
                    this.dropdownList.put(Boolean.TRUE, tempDropdownList.get(Boolean.TRUE));
                }
            }
        }
        // 处理级联下拉框
        for (Map.Entry<String, DropdownList> entry : topList.entrySet()) {
            DropdownList list = entry.getValue();
            CascadeDropdownData cascadeDropdownData = new CascadeDropdownData();
            cascadeDropdownData.setSheetName(entry.getKey());
            List<String> dropList = JSONArray.parseArray(list.valueJson(), String.class);
            String[] dropArr = new String[dropList.size()];
            dropList.toArray(dropArr);
            cascadeDropdownData.setParents(dropArr);
            Map<String, List<String>> cascadeData = new HashMap<>();
            Map<Integer, Integer> columnRelationships = new HashMap<>();
            columnRelationships.put(-1, list.index());
            cascadeDropdownData.setCascadeData(cascadeData);
            cascadeDropdownData.setColumnRelationships(columnRelationships);
            buildCascadeDropData(list.index(), Arrays.asList(dropArr), cascadeDropdownData, cascadeDropDownList);
            tempCascadeDropdownDataList.add(cascadeDropdownData);
        }
        if (null == this.cascadeDropdownDataList) {
            this.cascadeDropdownDataList = tempCascadeDropdownDataList;
        } else {
            this.cascadeDropdownDataList.addAll(tempCascadeDropdownDataList);
        }
        headers = new String[headerNames.size()];
        headerNames.toArray(headers);
    }

    private void buildCascadeDropData(int index, List<String> dropArr, CascadeDropdownData cascadeDropdownData, List<DropdownList> cascadeDropDownList) {
        DropdownList temp = null;
        for (DropdownList list : cascadeDropDownList) {
            if (list.parentIndex() == index) {
                temp = list;
                break;
            }
        }
        if (temp == null) {
            return;
        }
        cascadeDropdownData.getColumnRelationships().put(index, temp.index());
        Map<String, List<String>> data = (Map<String, List<String>>) JSONObject.parse(temp.valueJson());
        for (String s : dropArr) {
            if (data.containsKey(s)) {
                cascadeDropdownData.getCascadeData().put(s, data.get(s));
                buildCascadeDropData(temp.index(), data.get(s), cascadeDropdownData, cascadeDropDownList);
            }
        }
    }

    public static class ExcelUtilBuilder<T> {
        // 版本
        private ExcelVersion version;
        // 传入的模板excel
        private InputStream templateStream;
        // 用于读取的Excel文件
        private MultipartFile excelFile;
        // 用于存储读取的Excel数据的阻塞队列
        private BlockingQueue<T> blockingQueue;
        // 类类型
        private Class clazz;
        // 是否需要标题单元格样式
        private boolean needHeaderStyle;
        // 是否需要单元格样式
        private boolean needCellStyle;
        // 标题行样式
        private SelfCellStyle headStyle;
        // 普通行样式
        private SelfCellStyle cellStyle;
        // 标题
        private String[] headers;
        // 数据
        private List dataList;
        // 是否跳过第一行
        private boolean isSkipFirstRow = true;
        // 设置最大行数
        private Integer maxRow;
        // 设置最大列
        private Integer maxColumn;
        // 隐藏的列
        private List<Integer> hiddenColumns;
        // 只读列洗标
        private List<Integer> onlyReadColumns;
        // 下拉框数据,true代表按照行插入；false代表按照列插入；integer代表在多少行/列插入，String[]为数据
        Map<Boolean, Map<Integer, String[]>> dropdownList;
        // 级联下拉框数据
        private List<CascadeDropdownData> cascadeDropdownDataList;
        // 单元格名称
        private String sheetName;
        // 分区Service
        private DataPartition dataPartition;
        // 分区参数
        private Map partitionParam;

        /**
         * @return
         * @Description 构建指定版本的excel
         * @Date 2020/3/26 11:06
         * @Param [version, clazz]
         **/
        public ExcelUtilBuilder(ExcelVersion version, Class clazz) {
            this.version = version;
            this.clazz = clazz;
        }

        /**
         * @return
         * @Description 构建带模板的指定版本的excel
         * @Date 2020/3/26 11:06
         * @Param [excelStream, version, clazz]
         **/
        public ExcelUtilBuilder(InputStream excelStream, ExcelVersion version, Class clazz) {
            this.version = version;
            this.templateStream = excelStream;
            this.clazz = clazz;
        }

        /**
         * @return
         * @Description 构建Excel解析器
         * @Date 2020/3/26 11:07
         * @Param [excelFile excel文件, blockingQueue 保存数据的阻塞队列, clazz]
         **/
        public ExcelUtilBuilder(MultipartFile excelFile, BlockingQueue<T> blockingQueue, Class clazz) {
            if (excelFile == null) {
                throw new IllegalArgumentException("excel file not exist");
            }
            this.excelFile = excelFile;
            this.blockingQueue = blockingQueue;
            this.clazz = clazz;
        }

        public ExcelUtilBuilder setHeadStyle(SelfCellStyle headStyle) {
            this.needHeaderStyle = true;
            this.headStyle = headStyle;
            return this;
        }

        public ExcelUtilBuilder setCellStyle(SelfCellStyle cellStyle) {
            this.needCellStyle = true;
            this.cellStyle = cellStyle;
            return this;
        }

        public ExcelUtilBuilder setHeaders(String[] headers) {
            this.headers = headers;
            return this;
        }

        public ExcelUtilBuilder setDataList(List dataList) {
            this.dataList = dataList;
            return this;
        }

        public ExcelUtilBuilder setSkipFirstRow(boolean skipFirstRow) {
            isSkipFirstRow = skipFirstRow;
            return this;
        }

        public ExcelUtilBuilder setMaxRow(Integer maxRow) {
            this.maxRow = maxRow;
            return this;
        }

        public ExcelUtilBuilder setMaxColumn(Integer maxColumn) {
            this.maxColumn = maxColumn;
            return this;
        }

        public ExcelUtilBuilder setHiddenColumns(List<Integer> hiddenColumns) {
            this.hiddenColumns = hiddenColumns;
            return this;
        }

        public ExcelUtilBuilder setOnlyReadColumns(List<Integer> onlyReadColumns) {
            this.onlyReadColumns = onlyReadColumns;
            return this;
        }

        public ExcelUtilBuilder setDropdownList(Map<Boolean, Map<Integer, String[]>> dropdownList) {
            this.dropdownList = dropdownList;
            return this;
        }

        public ExcelUtilBuilder setCascadeDropdownDataList(List<CascadeDropdownData> cascadeDropdownDataList) {
            this.cascadeDropdownDataList = cascadeDropdownDataList;
            return this;
        }

        public ExcelUtilBuilder setSheetName(String sheetName) {
            this.sheetName = sheetName;
            return this;
        }

        public ExcelUtilBuilder setDefaultStyle() {
            this.needHeaderStyle = true;
            this.needCellStyle = true;
            return this;
        }

        public ExcelUtilBuilder setDataPartition(DataPartition dataPartition) {
            this.dataPartition = dataPartition;
            return this;
        }

        public ExcelUtilBuilder setPartitionParam(Map partitionParam) {
            this.partitionParam = partitionParam;
            return this;
        }

        public ExcelOperator build() {
            return new ExcelOperator(this);
        }
    }

    private void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
