package com.excel.example.demo.handlers;

import com.excel.example.demo.entity.CascadeDropdownData;
import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @Auther: Anntly
 * @Date: 2020/3/29 16:23
 * @Description: 级联下拉框工具
 */
public class CascadeDropDownListHandler {


    /**
     * @Author Anntly
     * @Description 创建级联下拉框，统一入口
     * @Date 2020/3/29 16:39
     * @Param [workbook, sheet, maxRow]
     * @return void
     **/
    public static void createCascadeDropDownList(List<CascadeDropdownData> cascadeDropdownDataList,
                                                 Workbook workbook, Sheet sheet,int maxRow){
        if (null != cascadeDropdownDataList && !cascadeDropdownDataList.isEmpty()) {
            for (CascadeDropdownData cascadeDropdownData : cascadeDropdownDataList) {
                createCascadeDropdownList(workbook, sheet, cascadeDropdownData, maxRow);
            }
        }
    }

    /**
     * 添加级联下拉数据
     *
     * @param workbook            workBook
     * @param displaySheet        用于展示的Sheet
     * @param cascadeDropdownData 填充的数据
     * @param maxRowNum           填充的数据的行数
     */
    public static void createCascadeDropdownList(Workbook workbook, Sheet displaySheet,
                                           CascadeDropdownData cascadeDropdownData, int maxRowNum) {
        // 顶级节点数组
        String[] parents = cascadeDropdownData.getParents();
        Sheet hiddenSheet = workbook.getSheet(cascadeDropdownData.getSheetName());
        if (hiddenSheet == null) {
            hiddenSheet = workbook.createSheet(cascadeDropdownData.getSheetName());
            // 设置Sheet 隐藏
            workbook.setSheetHidden(workbook.getSheetIndex(hiddenSheet), true);
            Map<String, List<String>> cascadeData = cascadeDropdownData.getCascadeData();
            int rowId = 0;
            // 设置第一行，存顶级父节点的信息
            Row firstRow = hiddenSheet.createRow(rowId++);
            firstRow.createCell(0).setCellValue("父节点");
            for (int i = 0; i < parents.length; i++) {
                Cell firstRowCell = firstRow.createCell(i + 1);
                firstRowCell.setCellValue(parents[i]);
            }
            // 创建所有父节点的数组
            Set<String> parentSet = cascadeData.keySet();
            String[] allParents = new String[parentSet.size()];
            parentSet.toArray(allParents);
            // 将具体的数据写入到每一行中，行开头为父级区域，后面是子区域。
            for (int i = 0; i < allParents.length; i++) {
                String key = allParents[i];
                List<String> son = cascadeData.get(key);
                Row row = hiddenSheet.createRow(rowId++);
                row.createCell(0).setCellValue(key);
                for (int j = 0; j < son.size(); j++) {
                    Cell cell = row.createCell(j + 1);
                    cell.setCellValue(son.get(j));
                }
                // 添加名称管理器
                String range = getRange(1, rowId, son.size());
                Name name = workbook.createName();
                //key不可重复
                name.setNameName(key);
                String formula = cascadeDropdownData.getSheetName() + "!" + range;
                name.setRefersToFormula(formula);
            }
        }
        Map<Integer, Integer> columnRelationships = cascadeDropdownData.getColumnRelationships();
        int firstParentIndex = columnRelationships.get(-1);
        // 顶级父节点下拉列表(单元格下拉框数据小于255个字节可以直接创建，大于255需要使用隐藏Sheet的方式)
        //createDropDownList(displaySheet, temp, 1, maxRowNum, firstParentIndex, firstParentIndex);
        DropDownListHandler dropDownListHandler = new DropDownListHandler();
        dropDownListHandler.createDropDownListWithHiddenSheet(displaySheet, 1, firstParentIndex, maxRowNum, firstParentIndex, parents, workbook, "hiddenParent" + cascadeDropdownData.getSheetName());
        for (Map.Entry<Integer, Integer> entry : columnRelationships.entrySet()) {
            // 跳过顶级节点
            if (entry.getKey() == -1) {
                continue;
            }
            int index = entry.getValue();
            int parentIndex = entry.getKey();
            String parentOffset = String.valueOf((char) ((int) 'A' + parentIndex));
            // poi的index从0开始
            setDataValidation(parentOffset, displaySheet, 1, maxRowNum, index);
        }
    }

    /**
     * 设置有效性
     *
     * @param offset 主影响单元格所在列，即此单元格由哪个单元格影响联动
     * @param sheet
     * @param rowNum 行数
     * @param colNum 列数
     */
    private static void setDataValidation(String offset, Sheet sheet, int rowNum, int lastRow, int colNum) {
        CellRangeAddressList rangeAddressList = new CellRangeAddressList(rowNum, lastRow, colNum, colNum);
        DataValidation cacse = null;
        if (sheet instanceof XSSFSheet || sheet instanceof SXSSFSheet) {
            DataValidationHelper dvHelper = sheet.getDataValidationHelper();
            String formulaStringXlsx = "INDIRECT($" + offset + (rowNum + 1) + ")";
            DataValidationConstraint dvConstraint = dvHelper.createFormulaListConstraint(formulaStringXlsx);
            cacse =  dvHelper.createValidation(dvConstraint, rangeAddressList);
        } else {
            String formulaString = "INDIRECT($" + offset + (rowNum) + ")";
            DVConstraint formula = DVConstraint.createFormulaListConstraint(formulaString);
            cacse = new HSSFDataValidation(rangeAddressList, formula);
        }
        cacse.createErrorBox("error", "请选择正确的选项");
        sheet.addValidationData(cacse);
    }


    /**
     * 计算formula
     *
     * @param offset   偏移量，如果给0，表示从A列开始，1，就是从B列
     * @param rowId    第几行
     * @param colCount 一共多少列
     * @return 如果给入参 1,1,10. 表示从B1-K1。最终返回 $B$1:$K$1
     */
    private static String getRange(int offset, int rowId, int colCount) {
        char start = (char) ('A' + offset);
        if (colCount <= 25) {
            char end = (char) (start + colCount - 1);
            return "$" + start + "$" + rowId + ":$" + end + "$" + rowId;
        } else {
            char endPrefix = 'A';
            char endSuffix = 'A';
            if ((colCount - 25) / 26 == 0 || colCount == 51) {// 26-51之间，包括边界（仅两次字母表计算）
                if ((colCount - 25) % 26 == 0) {// 边界值
                    endSuffix = (char) ('A' + 25);
                } else {
                    endSuffix = (char) ('A' + (colCount - 25) % 26 - 1);
                }
            } else {// 51以上
                if ((colCount - 25) % 26 == 0) {
                    endSuffix = (char) ('A' + 25);
                    endPrefix = (char) (endPrefix + (colCount - 25) / 26 - 1);
                } else {
                    endSuffix = (char) ('A' + (colCount - 25) % 26 - 1);
                    endPrefix = (char) (endPrefix + (colCount - 25) / 26);
                }
            }
            return "$" + start + "$" + rowId + ":$" + endPrefix + endSuffix + "$" + rowId;
        }
    }
}