package com.excel.example.demo.handlers;

import com.alibaba.fastjson.JSONObject;
import org.apache.poi.ss.formula.functions.T;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import java.lang.reflect.Field;
import java.util.concurrent.BlockingQueue;

public class SheetHandler<T> extends DefaultHandler {

    private SharedStringsTable sst;
    private String lastContents;
    private boolean nextIsString;
    private String cellPosition;
    private int fieldNum = -1;
    private Field[] declaredFields;
    private BlockingQueue<T> blockingQueue;
    private JSONObject json = new JSONObject();
    private Class clazz;

    public SheetHandler(SharedStringsTable sst, Class clazz, BlockingQueue<T> blockingQueue) {
        this.sst = sst;
        this.clazz = clazz;
        this.declaredFields = clazz.getDeclaredFields();
        this.blockingQueue = blockingQueue;
    }


    public XMLReader fetchSheetParser() throws SAXException {
        XMLReader parser =
                XMLReaderFactory.createXMLReader(
                        "com.sun.org.apache.xerces.internal.parsers.SAXParser"
                );
        ContentHandler handler = (ContentHandler) this;
        parser.setContentHandler(handler);
        return parser;
    }

    public void startElement(String uri, String localName, String name,
                             Attributes attributes) {
        if (name.equals("c")) {
            fieldNum = (++fieldNum) % declaredFields.length;
            cellPosition = attributes.getValue("r");
            String cellType = attributes.getValue("t");
            if (cellType != null && cellType.equals("s")) {
                nextIsString = true;
            } else {
                nextIsString = false;
            }
        }
        // 清除缓存内容
        lastContents = "";
    }


    public void endElement(String uri, String localName, String name)
            throws SAXException {
        if (nextIsString) {
            int idx = Integer.parseInt(lastContents);
            lastContents = new XSSFRichTextString(sst.getEntryAt(idx)).toString();
            nextIsString = false;
        }
        if (name.equals("v")) {
            //数据读取结束后，将单元格坐标,内容存入map中
            generatorObject();
        } else if (lastContents.equals("") &&
                (fieldNum == 0 || fieldNum == declaredFields.length - 1)
                && name.equals("c")) { // 当首列和末尾列为空的时候name为c没有v，需要额外处理空值
            generatorObject();
        }
    }

    // 生成实体类
    private void generatorObject() {
        if (!(cellPosition.length() == 2) || (cellPosition.length() == 2 && !"1".equals(cellPosition.substring(1)))) {//不保存第一行数据
            try {
                json.put(declaredFields[fieldNum].getName(), lastContents);
                if (fieldNum == declaredFields.length - 1) {
                    Object object = json.toJavaObject(clazz);
                    blockingQueue.put((T) object);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void characters(char[] ch, int start, int length)
            throws SAXException {
        lastContents += new String(ch, start, length);
    }
}