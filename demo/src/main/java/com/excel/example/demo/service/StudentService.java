package com.excel.example.demo.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.excel.example.demo.entity.Student;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Anntly
 * @since 2020-03-28
 */
public interface StudentService extends IService<Student> {

    // 查询所有
    List<Student> queryAll();

    // 查询所有数据(JSONArray格式)
    List<JSONObject> queryAll(String tableName);
}
