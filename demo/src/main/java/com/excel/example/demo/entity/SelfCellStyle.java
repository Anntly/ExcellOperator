package com.excel.example.demo.entity;

import org.apache.poi.ss.usermodel.*;

// excel单元格样式
public class SelfCellStyle {
    // 单元格高度
    private int height;
    // 单元格宽度
    private int width;
    // 单元格字体颜色
    private IndexedColors colors;
    // 字体大小
    private short fontSize;
    // 字体名称
    private String fontName;
    // 是否为斜体
    private boolean isItalic;
    // 是否为粗体
    private boolean isBold;
    // 左右居中方式
    private HorizontalAlignment aligh;
    // 上下居中方式
    private VerticalAlignment vertical;
    // 真正的样式
    private CellStyle cellStyle;

    public SelfCellStyle() {
    }

    public SelfCellStyle(int height, int width, IndexedColors colors, short fontSize, String fontName, boolean isItalic, boolean isBold, HorizontalAlignment aligh, VerticalAlignment vertical) {
        this.height = height;
        this.width = width;
        this.colors = colors;
        this.fontSize = fontSize;
        this.fontName = fontName;
        this.isItalic = isItalic;
        this.isBold = isBold;
        this.aligh = aligh;
        this.vertical = vertical;
    }

    public void buildCellStyle(Workbook workbook){
        CellStyle cellStyle = workbook.createCellStyle();
        Font font = workbook.createFont();
        cellStyle.setAlignment(HorizontalAlignment.forInt(getAligh().getCode()));
        // 设置垂直对齐的样式为居中对齐
        cellStyle.setVerticalAlignment(VerticalAlignment.forInt(getVertical().getCode()));
        font.setFontName(getFontName());//设置字体名称
        font.setFontHeightInPoints(getFontSize());//设置字号
        font.setItalic(isItalic());//设置是否为斜体
        font.setBold(isBold());//设置是否加粗
        font.setColor(getColors().index);//设置字体颜色
        cellStyle.setFont(font);
        this.cellStyle = cellStyle;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public IndexedColors getColors() {
        return colors;
    }

    public void setColors(IndexedColors colors) {
        this.colors = colors;
    }

    public short getFontSize() {
        return fontSize;
    }

    public void setFontSize(short fontSize) {
        this.fontSize = fontSize;
    }

    public String getFontName() {
        return fontName;
    }

    public void setFontName(String fontName) {
        this.fontName = fontName;
    }

    public boolean isItalic() {
        return isItalic;
    }

    public void setItalic(boolean italic) {
        isItalic = italic;
    }

    public boolean isBold() {
        return isBold;
    }

    public void setBold(boolean bold) {
        isBold = bold;
    }

    public HorizontalAlignment getAligh() {
        return aligh;
    }

    public void setAligh(HorizontalAlignment aligh) {
        this.aligh = aligh;
    }

    public VerticalAlignment getVertical() {
        return vertical;
    }

    public void setVertical(VerticalAlignment vertical) {
        this.vertical = vertical;
    }

    public CellStyle getCellStyle() {
        return cellStyle;
    }

    public void setCellStyle(CellStyle cellStyle) {
        this.cellStyle = cellStyle;
    }
}
