package com.excel.example.demo.enums;

/**
 * @Auther: Anntly
 * @Date: 2020/3/28 17:20
 * @Description: 导出Excel文件的方式
 */
public enum ExportExcelType {
    MULTIPLE_FILES, // 多文件单Sheet方式
    MULTIPLE_SHEETS, // 单文件多Sheet方式
    MULTIPLE_FILES_THREADS, // 多文件单Sheet方式(多线程实现，不支持下拉框等，需要2007的版本)
    ;
}