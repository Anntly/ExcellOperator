package com.excel.example.demo.mapper;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Anntly
 * @since 2020-03-28
 */
public interface StudentMapper extends BaseMapper<com.excel.example.demo.entity.Student>{

    // 查询结果直接使用JSONArray
    @ResultType(com.alibaba.fastjson.JSONObject.class)
    @Select("select * from ${tableName}")
    List<JSONObject> selectOneTable(@Param("tableName") String tableName);
}
