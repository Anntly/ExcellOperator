package com.excel.example.demo.annotations;

import com.excel.example.demo.enums.DropdownListType;

import java.lang.annotation.*;

/**
 * @Description 添加下拉列表
 * @Date 2020/3/27 15:18
 * @Param
 * @return
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
@Inherited
public @interface DropdownList {
    // 下拉框的数据,Json格式
    String valueJson() default "";
    // 下拉框是按行还是按列插入
    DropdownListType type() default DropdownListType.BY_COLUMN;
    // 对应的列数
    int index() default 0;
    // 是否为最顶部的父节点(最顶部没有父节点下标)
    boolean isTopParent() default false;
    // 级联下拉框，默认没有 -1；index即父下拉框列所在的下标，从0开始
    int parentIndex() default -1;
}
