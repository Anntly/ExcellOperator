package com.excel.example.demo.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.excel.example.demo.entity.CascadeDropdownData;
import com.excel.example.demo.entity.SelfCellStyle;
import com.excel.example.demo.entity.Student;
import com.excel.example.demo.enums.ExcelVersion;
import com.excel.example.demo.enums.ExportExcelType;
import com.excel.example.demo.service.StudentService;
import com.excel.example.demo.service.impl.DataPartitionStudent;
import com.excel.example.demo.utils.ExcelOperator;
import com.excel.example.demo.service.Process;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @Auther: 张文
 * @Date: 2020/3/20 15:28
 * @Description:
 */
@Controller
@RequestMapping("/excel")
public class ExcelController {

    @Autowired
    private StudentService studentService;

    @Autowired
    private DataPartitionStudent dataPartitionStudent;

    @RequestMapping("/page")
    public String excelPage() {
        return "excel";
    }

    @PostMapping("/import")
    @ResponseBody
    public String importExcel(MultipartFile file) throws Exception {
        BlockingQueue blockingQueue = new LinkedBlockingQueue<>();
        ExcelOperator excelOperator = new ExcelOperator.ExcelUtilBuilder<Student>(file, blockingQueue, Student.class)
                .setSkipFirstRow(true)
                .build();
        Long start = System.currentTimeMillis();
        // 参数.比如创建用户等，可以直接传入，由于使用到了多线程传入map的切记process方法中不要修改数据避免安全问题的产生
        Map<String, Object> param = new HashMap<>();
        param.put("create_name", "小刚");
        param.put("create_Time", new Date());
        // batcheNum是每10条执行一次process方法，由于使用的多线程读取阻塞队列，顺序不能保证
        excelOperator.paresExcelSingleThread(1000, param, new Process<List, Map>() {
            @Override
            public void process(List list, Map map) {
                // 上传数据,MybatisPlus后面指定的batchSize是该插件每1000条提交一次，否则默认是30条一次
                studentService.saveBatch(list,1000);
                // map参数可以根据自己要求对数据再进行处理
            }
        });
        Long end = System.currentTimeMillis();
        System.out.println("解析时间:" + (end - start));
        return null;
    }

    @RequestMapping("/export")
    @ResponseBody
    public void exportExcel(HttpServletResponse response) throws Exception {
        Student student = new Student();
        student.setId(0);
        student.setName("小红");
        //student.setBirthday(new Date());
        student.setCity("成都");
        student.setCountry("中国");
        student.setProvince("四川");
        student.setSex("男");
        List<Student> list = new ArrayList<>();
        Long initStart = System.currentTimeMillis();
        // 模拟数据
//        for (int i = 0; i < 10000; i++) {
//            Student temp = (Student) student.clone();
//            //student.setId(i);
//            student.setName("小红" + i);
//            list.add(temp);
//        }
        // 查询数据
        List<JSONObject> students = studentService.queryAll("tb_student");
        // 数据分区的Service
        // 用于分区查询的参数
        Map<String,Object> partitionParam = new HashMap<>();
        partitionParam.put("sex","男");
        // 性别下拉框,可以在实体类配置注解
        Map<Boolean, Map<Integer, String[]>> dropdownList = new HashMap<>();
        Map<Integer, String[]> sexMap = new HashMap<>();
        sexMap.put(2, new String[]{"男", "女"});
        dropdownList.put(Boolean.FALSE, sexMap);
        // 省市县级联下拉框,可以在实体类配置注解.(由于使用了递归,建议多级的级联下拉框可以自己查询出来按照格式拼接一下，
        // 数据不是经常改变的话，可以结合缓存进行优化)
        List<CascadeDropdownData> cascadeDropdownDataList = new ArrayList<>();
        CascadeDropdownData cascadeDropdownData = new CascadeDropdownData();
        cascadeDropdownData.setSheetName("国家省市");
        cascadeDropdownData.setParents(new String[]{"中国"});
        Map<String, List<String>> cascadeData = new HashMap<>();
        cascadeData.put("中国", Arrays.asList("北京", "上海", "广东", "四川"));
        cascadeData.put("北京", Arrays.asList("东城区", "西城区", "朝阳区", "丰台区"));
        cascadeData.put("上海", Arrays.asList("黄浦区", "徐汇区", "长宁区", "静安区"));
        cascadeData.put("广东", Arrays.asList("广州市", "韶关市", "深圳市", "珠海市"));
        cascadeData.put("四川", Arrays.asList("成都市", "自贡市", "攀枝花市", "泸州市"));
        cascadeDropdownData.setCascadeData(cascadeData);
        Map<Integer, Integer> columnRelationships = new HashMap<>();
        columnRelationships.put(-1, 4);
        columnRelationships.put(4, 5);
        columnRelationships.put(5, 6);
        cascadeDropdownData.setColumnRelationships(columnRelationships);
        cascadeDropdownDataList.add(cascadeDropdownData);
        Long initEnd = System.currentTimeMillis();
        // 样式
        SelfCellStyle headerStyle = new SelfCellStyle();
        SelfCellStyle cellStyle = new SelfCellStyle();
        headerStyle.setAligh(HorizontalAlignment.CENTER);
        headerStyle.setVertical(VerticalAlignment.CENTER);
        headerStyle.setFontName("宋体");
        headerStyle.setFontSize((short) 14);
        headerStyle.setItalic(false);
        headerStyle.setBold(true);
        headerStyle.setColors(IndexedColors.RED);
        headerStyle.setWidth(25);
        headerStyle.setHeight(30);
        cellStyle.setAligh(HorizontalAlignment.CENTER);
        cellStyle.setVertical(VerticalAlignment.CENTER);
        cellStyle.setFontName("宋体");
        cellStyle.setFontSize((short) 14);
        cellStyle.setItalic(false);
        cellStyle.setBold(true);
        cellStyle.setColors(IndexedColors.BLUE);
        cellStyle.setWidth(25);
        cellStyle.setHeight(12);
        System.out.println("初始化数据:" + (initEnd - initStart));
        ExcelOperator excelOperator = new ExcelOperator.ExcelUtilBuilder<Student>(ExcelVersion.V2007, Student.class)
                // 设置数据(数据量大不推荐使用，如果要使用把downloadExcel代码里面注释的代码进行更换即可)
                .setDataList(students)
                // 使用实现的分区Service来进行分区导出
                //.setDataPartition(dataPartitionStudent)
                // 分区Service查询需要的参数
                //.setPartitionParam(partitionParam)
                // sheet名称，多个sheet就在后面加数字
                //.setSheetName("学生页")
                // 下拉框数据，按格式要求填充
                //.setDropdownList(dropdownList)
                // 级联下拉框数据
                //.setCascadeDropdownDataList(cascadeDropdownDataList)
                // 设置单元格样式为默认，不设置就没有；可以自己创建
                .setDefaultStyle()
                // 设置自定义的标题格式
                //.setHeadStyle(headerStyle)
                // 设置自定义的普通单元格格
                //.setCellStyle(cellStyle)
                // 读取的时候是否跳过首行,默认跳过
                //.setSkipFirstRow(true)
                // 可以不使用注解，自己注入标题行名称，以注入的优先级高
                //.setHeaders()
                // 设定隐藏的列
                //.setHiddenColumns()
                // 设定excel最大的列，不设定以ExcelVersion中的为准
                //.setMaxColumn()
                // 设定excel最大的行，不设定以ExcelVersion中的为准
                //.setMaxRow()
                // 设置只读列(未实现，效果不理想就没弄，以后有机会会补上)
                //.setOnlyReadColumns()
                .build();
        excelOperator.downloadExcel("学生表", response, ExportExcelType.MULTIPLE_SHEETS);
        Long downloadEnd = System.currentTimeMillis();
        System.out.println("导出时间:" + (downloadEnd - initEnd));
    }
}