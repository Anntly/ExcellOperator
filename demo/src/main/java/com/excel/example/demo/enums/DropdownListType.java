package com.excel.example.demo.enums;

public enum DropdownListType {
    BY_ROW,
    BY_COLUMN
    ;

    DropdownListType() {
    }
}
