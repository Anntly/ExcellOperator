package com.excel.example.demo.service;

@FunctionalInterface
public interface Process<T,R> {

    void process(T t,R r);
}
