package com.excel.example.demo.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.excel.example.demo.entity.Student;
import com.excel.example.demo.service.DataPartition;

import com.excel.example.demo.service.StudentService;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

/**
 * @Auther: 张文
 * @Date: 2020/3/28 18:49
 * @Description:
 */
@Service
public class DataPartitionStudent implements DataPartition {

    @Autowired
    private StudentService studentService;

    @Override
    public int dataCount(Map<String, Object> param) {
        Student student = new Student();
        int count = 0;
        try {
            BeanUtils.populate(student,param);
            QueryWrapper<Student> queryWrapper = new QueryWrapper<>(student);
            count = studentService.count(queryWrapper);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return count;
    }

    @Override
    public JSONArray getPageData(Map<String, Object> param, int page, int size) {
        IPage<Student> iPage = new Page<>(page,size);
        Student student = new Student();
        try {
            BeanUtils.populate(student,param);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        QueryWrapper<Student> wrapper = new QueryWrapper<>(student);
        IPage<Student> pageData = studentService.page(iPage,wrapper);
        List<Student> records = pageData.getRecords();
        // 需要将数据转换为JSONArray再使用，因为使用反射解析数据还是比较慢的
        JSONArray array= JSONArray.parseArray(JSON.toJSONString(records));
        return array;
    }
}