package com.excel.example.demo.service.exportStrategies;

import com.alibaba.fastjson.JSONObject;
import com.excel.example.demo.entity.CascadeDropdownData;
import com.excel.example.demo.entity.SelfCellStyle;
import com.excel.example.demo.enums.ExcelVersion;
import com.excel.example.demo.handlers.CascadeDropDownListHandler;
import com.excel.example.demo.handlers.DropDownListHandler;
import com.excel.example.demo.handlers.StyleHandler;
import com.excel.example.demo.handlers.ValueHandler;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.zip.ZipOutputStream;

/**
 * @Author Anntly
 * @Description  Excel生成策略
 * @Date 2020/3/30 11:52
 * @Param
 * @return
 **/
public abstract class AbstractExcelGeneratorStrategy {

    protected List dataList;
    protected InputStream templateStream;
    protected volatile String sheetName;
    protected Class clazz;
    protected String[] headers;
    protected ExcelVersion version;
    protected boolean needHeaderStyle;
    protected boolean needCellStyle;
    protected SelfCellStyle headStyle = new SelfCellStyle();
    protected SelfCellStyle cellStyle = new SelfCellStyle();
    protected volatile Integer maxRow;
    protected volatile Integer maxColumn;
    protected Map<String, DateTimeFormatter> dateFormatterPattern;
    protected Field[] declaredFields;
    protected static Pattern p = Pattern.compile("^//d+(//.//d+)?$");

    public AbstractExcelGeneratorStrategy(InputStream templateStream, String sheetName, Class clazz, String[] headers, ExcelVersion version, boolean needHeaderStyle, boolean needCellStyle, SelfCellStyle headStyle, SelfCellStyle cellStyle, Integer maxRow, Integer maxColumn, Map<String, DateTimeFormatter> dateFormatterPattern, Field[] declaredFields) {
        this.templateStream = templateStream;
        this.sheetName = sheetName;
        this.clazz = clazz;
        this.headers = headers;
        this.version = version;
        this.needHeaderStyle = needHeaderStyle;
        this.needCellStyle = needCellStyle;
        this.headStyle = headStyle;
        this.cellStyle = cellStyle;
        this.maxRow = maxRow;
        this.maxColumn = maxColumn;
        this.dateFormatterPattern = dateFormatterPattern;
        this.declaredFields = declaredFields;
    }

    public AbstractExcelGeneratorStrategy(List dataList, InputStream templateStream, String sheetName,
                                          Class clazz, String[] headers, ExcelVersion version,
                                          boolean needHeaderStyle, boolean needCellStyle,
                                          SelfCellStyle headStyle, SelfCellStyle cellStyle,
                                          Integer maxRow, Integer maxColumn,
                                          Map<String, DateTimeFormatter> dateFormatterPattern,
                                          Field[] declaredFields) {
        this.dataList = dataList;
        this.templateStream = templateStream;
        this.sheetName = sheetName;
        this.clazz = clazz;
        this.headers = headers;
        this.version = version;
        this.needHeaderStyle = needHeaderStyle;
        this.needCellStyle = needCellStyle;
        this.headStyle = headStyle;
        this.cellStyle = cellStyle;
        this.maxRow = maxRow;
        this.maxColumn = maxColumn;
        this.dateFormatterPattern = dateFormatterPattern;
        this.declaredFields = declaredFields;
    }

    /**
     * @Description  生成Excel,单Excel
     * @Date 2020/3/30 11:55
     * @Param [outputStream]
     * @return void
     **/
    public abstract void generateExcel(OutputStream outputStream) throws Exception;


    /**
     * @Description  生成Excel,多Excel
     * @Date 2020/3/30 11:55
     * @Param [outputStream]
     * @return void
     **/
    public abstract void generateExcelMultipleFile(ZipOutputStream zop) throws Exception;
    /**
     * @Description 初始化WorkBook
     * @Date 2020/3/30 11:33
     * @Param []
     * @return org.apache.poi.ss.usermodel.Workbook
     **/
    protected Workbook initWorkBook() throws IOException{
        Workbook workbook = null;
        if(version == null){
            throw new IllegalArgumentException("Invalid excel version");
        }
        if (templateStream == null){
            if(ExcelVersion.V2003.equals(version)){
                workbook = new HSSFWorkbook();
            }else if(ExcelVersion.V2007.equals(version)){
                workbook = new XSSFWorkbook();
            }
        }else {
            templateStream.reset();
            if(ExcelVersion.V2003.equals(version)){
                workbook = new HSSFWorkbook(templateStream);
            }else if(ExcelVersion.V2007.equals(version)){
                workbook = new XSSFWorkbook(templateStream);
            }
        }
        return workbook;
    }

    /**
     * @Description 初始化WorkBook
     * @Date 2020/3/30 11:33
     * @Param []
     * @return
     **/
    protected SXSSFWorkbook initSxssfWorkBook() throws IOException{
        if(!ExcelVersion.V2007.equals(version)){
            throw new IllegalArgumentException("Invalid excel version");
        }
        SXSSFWorkbook workbook = null;
        if (templateStream == null){
            if(ExcelVersion.V2007.equals(version)){
                XSSFWorkbook temp = new XSSFWorkbook();
                workbook = new SXSSFWorkbook(temp);
                workbook.setCompressTempFiles(true);
                return workbook;
            }
        }else {
            templateStream.reset();
           if(ExcelVersion.V2007.equals(version)){
               XSSFWorkbook temp = new XSSFWorkbook(templateStream);
               workbook = new SXSSFWorkbook(temp);
               workbook.setCompressTempFiles(true);
               return workbook;
            }
        }
        return workbook;
    }

    /**
     * @Description 初始化单元格样式
     * @Date 2020/3/30 15:10
     * @Param [workbook]
     * @return com.excel.example.demo.entity.SelfCellStyle
     **/
    protected void initCellStyle(Workbook workbook){
        // 初始化标题行样式
        if (this.needHeaderStyle) {
            this.headStyle = StyleHandler.initHeaderStyle(workbook,headStyle);
        }
        if (this.needCellStyle) {
            // 初始化普通单元格样式
            this.cellStyle = StyleHandler.initCellStyle(workbook,cellStyle);
        }
    }

    /**
     * 初始化标题行
     *
     * @param sheet
     * @param headerRow
     */
    protected void initHeaderRow(Sheet sheet, Row headerRow, SelfCellStyle style) {
        int width = 0;
        if (needHeaderStyle) {
            headerRow.setHeightInPoints(headStyle.getHeight());
            width = 256 * headStyle.getWidth() + 184;
        }
        if (null != headers && headers.length > 0) {
            for (int i1 = 0; i1 < headers.length; i1++) {
                Cell headerCell = headerRow.createCell(i1);
                headerCell.setCellValue(headers[i1]);
                if (needHeaderStyle) {
                    headerCell.setCellStyle(style.getCellStyle());
                    sheet.setColumnWidth(i1, width);
                }
            }
        }
    }

    /**
     * @Description 处理下拉框、级联下拉框、隐藏列
     * @Date 2020/3/30 11:46
     * @Param [rowNum, dropDownListHandler, cascadeDropdownDataList, wb, sheet]
     * @return void
     **/
    protected void handleOrhersMuliteFiles(int rowNum, DropDownListHandler dropDownListHandler,
                                        List<CascadeDropdownData> cascadeDropdownDataList,
                                        List<Integer> hiddenColumns,
                                        Workbook wb, Sheet sheet) {
        dropDownListHandler.createDropDownList(wb, sheet, rowNum, headers.length);
        // 创建级联下拉列表
        CascadeDropDownListHandler.createCascadeDropDownList(cascadeDropdownDataList, wb, sheet, rowNum);
        // 隐藏列
        hideColumns(sheet,hiddenColumns);
    }

    /**
     * 隐藏列
     * @param sheet
     * @param hiddenColumns
     */
    protected void hideColumns(Sheet sheet,List<Integer> hiddenColumns) {
        if (null != hiddenColumns && hiddenColumns.size() > 0) {
            for (Integer hiddenColumn : hiddenColumns) {
                sheet.setColumnHidden(hiddenColumn, true);
            }
        }
    }

    /**
     * @Description 检查模板是否正确
     * @Date 2020/3/30 11:23
     * @Param [declaredFields]
     * @return void
     **/
    protected final void checkHeaderIsRight() {
        if (null == declaredFields || declaredFields.length != headers.length) {
            throw new RuntimeException("标题与实体类字段长度不匹配");
        }
    }

    /**
     * 计算需要的sheet或者excel数量
     *
     * @return
     */
    protected final int computeSheetOrWorkbookCount(int dataSize) {
        int sheetCount = 1;
        if (dataSize > maxRow) {
            if (dataSize % maxRow > 0) {
                sheetCount = dataSize / maxRow + 1;
            } else {
                sheetCount = dataSize / maxRow;
            }
        }
        return sheetCount;
    }

    /**
     * @Description  数据量大使用SxssWorkBook代替
     * @Date 2020/3/29 20:30
     * @Param [size, version, workbook]
     * @return org.apache.poi.ss.usermodel.Workbook
     **/
    protected final Workbook isBigData(Workbook workbook){
        if (dataList.size() > 80000 && ExcelVersion.V2007.equals(version)) {
            SXSSFWorkbook sxssfWorkbook = new SXSSFWorkbook((XSSFWorkbook) workbook);
            sxssfWorkbook.setCompressTempFiles(true);
            return sxssfWorkbook;
        }else {
            return workbook;
        }
    }

    /**
     * @Description excel填充一行数据
     * @Date 2020/3/30 11:27
     * @Param [p, matcher, richString, row, o, cellStyle, dataType]
     * @return void
     **/
    protected final void fillRowData(Pattern p,Row row, Object o,
                          SelfCellStyle cellStyle, boolean dataType) throws Exception {
        if (needCellStyle) {
            row.setHeightInPoints(cellStyle.getHeight());
        }
        for (int i1 = 0; i1 < declaredFields.length; i1++) {
            Cell cell = row.createCell(i1);
            if (needCellStyle) {
                cell.setCellStyle(cellStyle.getCellStyle());
            }
            Object value;
            if(dataType){
                JSONObject jo = (JSONObject) JSONObject.toJSON(o);
                value = jo.get(declaredFields[i1].getName());
            }else {
                value = ValueHandler.getProperty(o, declaredFields[i1].getName());
            }
            String textValue = null;
            textValue = ValueHandler.getString(cell, value, textValue, dateFormatterPattern.get(declaredFields[i1].getName()));
            if (null != textValue) {
                if (p.matcher(textValue).matches()) {
                    // 是数字当作double处理
                    cell.setCellValue(Double.parseDouble(textValue));
                } else {
                    if (ExcelVersion.V2003.equals(version)) {
                        cell.setCellValue(new HSSFRichTextString(textValue));
                    } else {
                        cell.setCellValue(new XSSFRichTextString(textValue));
                    }

                }
            }
        }
    }

    protected final void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
