package com.excel.example.demo.handlers;

import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFDataValidation;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.util.Map;

/**
 * @Auther: Anntly
 * @Date: 2020/3/29 13:43
 * @Description: 添加下拉框
 */
public class DropDownListHandler {

    // 创建下拉框时的第一行
    private int firstRow;
    // 创建下拉框时的第一列
    private int firstColumn;
    // 下拉框数据 true代表按照行插入；false代表按照列插入；integer代表在多少行/列插入，String[]为数据
    private Map<Boolean, Map<Integer, String[]>> dropdownList;

    public DropDownListHandler() {
    }

    public DropDownListHandler(int firstRow, int firstColumn, Map<Boolean, Map<Integer, String[]>> dropdownList) {
        this.firstRow = firstRow;
        this.firstColumn = firstColumn;
        this.dropdownList = dropdownList;
    }

    /**
     * @Description 创建下拉框,统一入口
     * @Date 2020/3/29 14:09
     * @Param []
     * @return void
     **/
    public void createDropDownList(Workbook workbook,Sheet sheet,int rowNum,int columnNum){
        if (dropdownList == null){
            return;
        }
        for (Map.Entry<Boolean,Map<Integer, String[]>> entry : dropdownList.entrySet()){
            if(entry.getKey()){
                createDropDownListByRow(sheet,entry.getValue(),columnNum);
            }else {
                createDropDownListByColumn(workbook,entry.getValue(),sheet,rowNum);
            }
        }
    }

    /**
     * 按列创建下拉框
     *
     * @param workbook
     * @param sheet
     * @param rowNum 需要下拉框的最大行数
     */
    private void createDropDownListByColumn(Workbook workbook,Map<Integer, String[]> dataRow, Sheet sheet, int rowNum) {
        if(workbook == null || sheet == null || dataRow == null){
            return;
        }
        int count = 0;
        for (Map.Entry<Integer, String[]> entry : dataRow.entrySet()) {
            count++;
            // 根据下拉框数据大小选择合适的方法
            //createDropDownList(sheet, entry.getValue(), 1, maxRow, entry.getKey(), entry.getKey());
            createDropDownListWithHiddenSheet(sheet, firstRow, entry.getKey(), rowNum, entry.getKey(), entry.getValue(), workbook, "hidden" + count);
        }
    }

    /**
     * 按行创建下拉框
     *
     * @param sheet
     * @param coulmnNum 需要下拉框最大列的数量
     */
    private void createDropDownListByRow(Sheet sheet,Map<Integer, String[]> dataRow, int coulmnNum) {
        if(sheet == null || dataRow == null){
            return;
        }
        // 按行处理数据
        for (Map.Entry<Integer, String[]> entry : dataRow.entrySet()) {
            // 根据下拉框数据大小选择合适的方法;如果单个下拉框数据大于255个字节选择 createDropDownListWithHiddenSheet()
            createDropDownList(sheet, entry.getValue(), entry.getKey(), entry.getKey(), firstColumn, coulmnNum);
        }
    }


    /**
     * 创建下拉列表选项(单元格下拉框数据小于255字节时使用)
     *
     * @param sheet    所在Sheet页面
     * @param values   下拉框的选项值
     * @param firstRow 起始行（从0开始）
     * @param lastRow  终止行（从0开始）
     * @param firstCol 起始列（从0开始）
     * @param lastCol  终止列（从0开始）
     */
    public void createDropDownList(Sheet sheet, String[] values, int firstRow, int lastRow, int firstCol, int lastCol) {
        DataValidationHelper helper = sheet.getDataValidationHelper();
        CellRangeAddressList addressList = new CellRangeAddressList(firstRow, lastRow, firstCol, lastCol);
        DataValidationConstraint constraint = helper.createExplicitListConstraint(values);
        DataValidation dataValidation = helper.createValidation(constraint, addressList);
        handleCompatible(dataValidation);
        sheet.addValidationData(dataValidation);
    }

    /**
     * 隐藏Sheet方式创建下拉框(单元格下拉框数据大于255字节时使用)
     *
     * @param sheet  需要添加下拉框的Sheet
     * @param firstRow 起始行
     * @param firstCol 其实列
     * @param endRow   终止行
     * @param endCol   终止列
     * @param dataArray  下拉框数组
     * @param wbCreat    所在excel的WorkBook，用于创建隐藏Sheet
     * @param hidddenSheetName    隐藏Sheet的名称
     * @return
     */
    public void createDropDownListWithHiddenSheet(Sheet sheet, int firstRow,
                                                  int firstCol, int endRow,
                                                  int endCol, String[] dataArray,
                                                  Workbook wbCreat,
                                                  String hidddenSheetName) {
        Sheet hidden = wbCreat.getSheet(hidddenSheetName);
        // 如果是单文件多Sheet的模式就要保证隐藏Sheet只能创建一个
        if (hidden == null) {
            hidden = wbCreat.createSheet(hidddenSheetName);
            Cell cell = null;
            for (int i = 0, length = dataArray.length; i < length; i++) {
                String name = dataArray[i];
                Row row = hidden.createRow(i);
                cell = row.createCell(0);
                cell.setCellValue(name);
            }
            Name namedCell = wbCreat.createName();
            namedCell.setNameName(hidddenSheetName);
            namedCell.setRefersToFormula(hidddenSheetName + "!$A$1:$A$" + dataArray.length);
            //sheet设置为隐藏
            wbCreat.setSheetHidden(wbCreat.getSheetIndex(hidden), true);
        }
        //加载数据,将名称为hidden的
        DataValidationConstraint constraint = null;
        // 设置数据有效性加载在哪个单元格上,四个参数分别是：起始行、终止行、起始列、终止列
        CellRangeAddressList addressList = new CellRangeAddressList(firstRow, endRow, firstCol,
                endCol);
        // 创建 DataValidation
        DataValidation validation = null;
        if (sheet instanceof XSSFSheet || sheet instanceof SXSSFSheet) {
            DataValidationHelper dvHelper = sheet.getDataValidationHelper();
            constraint = dvHelper.createFormulaListConstraint(hidddenSheetName);
            validation = dvHelper.createValidation(constraint, addressList);
        } else {
            constraint = DVConstraint.createFormulaListConstraint(hidddenSheetName);
            validation = new HSSFDataValidation(addressList, constraint);
        }
        handleCompatible(validation);
        sheet.addValidationData(validation);
    }

    // Excel兼容性问题
    private void handleCompatible(DataValidation dataValidation) {
        if (dataValidation instanceof HSSFDataValidation ) {
            dataValidation.setSuppressDropDownArrow(false);
        } else {
            dataValidation.setSuppressDropDownArrow(true);
            dataValidation.setShowErrorBox(true);
        }
    }
}