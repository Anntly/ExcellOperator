package com.excel.example.demo.service.exportStrategies.impl;

import com.alibaba.fastjson.JSONObject;
import com.excel.example.demo.entity.CascadeDropdownData;
import com.excel.example.demo.entity.SelfCellStyle;
import com.excel.example.demo.enums.ExcelVersion;
import com.excel.example.demo.handlers.DropDownListHandler;
import com.excel.example.demo.handlers.StyleHandler;
import com.excel.example.demo.service.exportStrategies.AbstractExcelGeneratorStrategy;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;


import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @Auther: 张文
 * @Date: 2020/3/30 13:49
 * @Description: 多线程导出多文件
 */
public class ExcelGeneratoMultipleFileThreadsStrategy extends AbstractExcelGeneratorStrategy {

    private String fileName;
    // 隐藏的列
    private List<Integer> hiddenColumns;
    // 下拉框数据,true代表按照行插入；false代表按照列插入；integer代表在多少行/列插入，String[]为数据
    private Map<Boolean, Map<Integer, String[]>> dropdownList;
    // 级联下拉框
    private List<CascadeDropdownData> cascadeDropdownDataList;
    // 本机的CPU核心数量
    static final int nThreads = Runtime.getRuntime().availableProcessors();
    private boolean needOthers;

    public ExcelGeneratoMultipleFileThreadsStrategy(List dataList,String fileName,InputStream templateStream, String sheetName, Class clazz,
                                                    String[] headers, ExcelVersion version, boolean needHeaderStyle,
                                                    boolean needCellStyle, SelfCellStyle headStyle, SelfCellStyle cellStyle,
                                                    Integer maxRow, Integer maxColumn, Map<String, DateTimeFormatter> dateFormatterPattern,
                                                    Field[] declaredFields, List<Integer> hiddenColumns,
                                                    Map<Boolean, Map<Integer, String[]>> dropdownList,
                                                    List<CascadeDropdownData> cascadeDropdownDataList) {
        super(templateStream, sheetName, clazz, headers, version, needHeaderStyle, needCellStyle, headStyle, cellStyle, maxRow, maxColumn, dateFormatterPattern, declaredFields);
        this.dataList = dataList;
        this.fileName = fileName;
        this.hiddenColumns = hiddenColumns;
        this.dropdownList = dropdownList;
        this.cascadeDropdownDataList = cascadeDropdownDataList;
        if (!(dataList.get(0) instanceof JSONObject)) {
            throw new UnsupportedOperationException("请使用JSONArray格式的数据");
        }
        if (!ExcelVersion.V2007.equals(version)) {
            throw new UnsupportedOperationException("请使用xlsx格式");
        }
    }

    @Override
    public void generateExcel(OutputStream outputStream) throws Exception {
        throw new UnsupportedOperationException("不支持单文件导出");
    }

    @Override
    public void generateExcelMultipleFile(ZipOutputStream zop) throws Exception {
        ExecutorService executorService = null;
        try {
            super.checkHeaderIsRight();
            needOthers = hiddenColumns != null || dropdownList != null || cascadeDropdownDataList != null;
            int excelCount = computeSheetOrWorkbookCount(dataList.size());
            DropDownListHandler dropDownListHandler = new DropDownListHandler(1, 0, this.dropdownList);
            // 计算线程数量
            int threadNum = nThreads > excelCount ? excelCount + 1 : nThreads + 1;
            executorService = Executors.newFixedThreadPool(threadNum);
            CountDownLatch countDownLatch = new CountDownLatch(excelCount);
            //将各个excel文件的数量进行等差处理，避免线程都阻塞在写入IO
//        List<Integer> split = split(dataList.size(), excelCount);
//        int pre = 0;
//        for (int i = 0; i < split.size(); i++) {
//            List<T> excelDataList = null;
//            if(i == 0){
//                excelDataList = dataList.subList(pre,split.get(i));
//                pre = split.get(i);
//                executorService.execute(new ExcelFileGenerator(excelDataList,fileName+i,this.workbook,zop,countDownLatch,declaredFields));
//            }else {
//                excelDataList = dataList.subList(pre,split.get(i) + pre);
//                pre = split.get(i) + pre;
//                executorService.execute(new ExcelFileGenerator(excelDataList,fileName+i,null,zop,countDownLatch,declaredFields));
//            }
//        }
            int dataCount = dataList.size();
            for (int i = 0; i < excelCount; i++) {
                int pageSize = Math.min(maxRow, dataCount);
                dataCount = dataCount - (maxRow);
                List excelDataList = null;
                SXSSFWorkbook wb = super.initSxssfWorkBook();
                if(i == 0){
                    super.initCellStyle(wb);
                }
                excelDataList = dataList.subList(i * maxRow, i * maxRow + pageSize);
                executorService.execute(new ExcelFileGenerator(excelDataList, fileName + i, wb, zop, countDownLatch));

            }
            countDownLatch.await();
        } finally {
            // 关闭线程池
            if(executorService != null){
                executorService.shutdown();
            }
            zop.flush();
            close(zop);
        }
    }

    // 只处理xlsx格式的文件
    class ExcelFileGenerator implements Runnable {

        private CountDownLatch countDownLatch;
        private List dataList;
        private String fileName;
        private SXSSFWorkbook workbook;
        private ZipOutputStream zop;

        public ExcelFileGenerator(List dataList, String fileName, SXSSFWorkbook workbook,
                                  ZipOutputStream zop, CountDownLatch countDownLatch) {
            this.dataList = dataList;
            this.fileName = fileName;
            this.workbook = workbook;
            this.zop = zop;
            this.countDownLatch = countDownLatch;
        }


        @Override
        public void run() {
            // 创建WorkBook、Sheet
            SXSSFSheet sxssfSheet;
            ByteArrayOutputStream bos = null;
            if (null != templateStream) {
                sxssfSheet = workbook.getSheetAt(0);
            } else {
                sxssfSheet = workbook.createSheet(sheetName);
            }
            // 创建标题行
            Row headerRow = sxssfSheet.createRow(0);
            // 不同workBook的样式要不同
            SelfCellStyle wbHeadCellStyle = null;
            if (needHeaderStyle) {
                wbHeadCellStyle = StyleHandler.copyCellStyle(headStyle, workbook, true);
            }
            initHeaderRow(sxssfSheet, headerRow, wbHeadCellStyle);
            // 初始化数据
            try {
                int index = 1;
                // 单元格样式
                SelfCellStyle wbCellStyle = null;
                if (needCellStyle) {
                    wbCellStyle = StyleHandler.copyCellStyle(cellStyle, workbook, false);
                }
                for (Object obj : this.dataList) {
                    Row row = sxssfSheet.createRow(index);
                    index++;
                    fillRowData(p, row, obj, wbCellStyle, true);
                }
                if(needOthers){
                    DropDownListHandler dropDownListHandler = new DropDownListHandler(1, 0, dropdownList);
                    handleOrhersMuliteFiles(dataList.size(), dropDownListHandler, cascadeDropdownDataList, hiddenColumns, workbook, sxssfSheet);
                }
                // 写到压缩文件流需要加锁
                bos = new ByteArrayOutputStream();
                workbook.write(bos);
                synchronized (zop.getClass()) {
                    zop.putNextEntry(new ZipEntry(fileName + version.getSuffix()));
                    bos.writeTo(zop);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                countDownLatch.countDown();
                workbook.dispose();
                close(bos);
            }
        }
    }
}