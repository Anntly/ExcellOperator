package com.excel.example.demo.entity;

import java.util.List;
import java.util.Map;

/**
 * excel 级联下拉需要的入参
 */
public class CascadeDropdownData {
    // 隐藏Sheet，用于存放级联下拉内容
    String SheetName;
    // 最顶层数据
    String[] parents;
    // 级联数据，key:父节点 value:父节点对应的子节点;需要按照顺序加入
    Map<String, List<String>> cascadeData;
    // 级联列下标 key:父节点对应的列 value:当前节点对应的列  key=-1代表为最顶级的节点对应的列
    // 下标从0开始计算
    Map<Integer, Integer> columnRelationships;

    public String getSheetName() {
        return SheetName;
    }

    public void setSheetName(String sheetName) {
        SheetName = sheetName;
    }

    public String[] getParents() {
        return parents;
    }

    public void setParents(String[] parents) {
        this.parents = parents;
    }

    public Map<String, List<String>> getCascadeData() {
        return cascadeData;
    }

    public void setCascadeData(Map<String, List<String>> cascadeData) {
        this.cascadeData = cascadeData;
    }

    public Map<Integer, Integer> getColumnRelationships() {
        return columnRelationships;
    }

    public void setColumnRelationships(Map<Integer, Integer> columnRelationships) {
        this.columnRelationships = columnRelationships;
    }
}
