package com.excel.example.demo.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.List;
import java.util.Map;

/**
 * @Author Anntly
 * @Description 用于获取数据，大量的数据不可能一次性全部放在内存中再进行导出，使用分页查询获取数据
 *              注意： 分区要使用物理分区
 * @Date 2020/3/28 17:08
 * @Param
 * @return
 **/
public interface DataPartition {

    /**
     * @Description 获取要导出数据的总大小，用于计算分页
     * @Date 2020/3/28 18:04
     * @Param [param]
     * @return int
     **/
    int dataCount(Map<String,Object> param);
    /**
     * @Description
     * @Date 2020/3/28 17:50
     * @Param [param] 查询需要的参数
     * @Param [page]  查询的页数
     * @Param [size]  该页的数量
     **/
    JSONArray getPageData(Map<String,Object> param, int page, int size);
}
