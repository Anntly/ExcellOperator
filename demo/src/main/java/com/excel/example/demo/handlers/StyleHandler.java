package com.excel.example.demo.handlers;

import com.excel.example.demo.entity.SelfCellStyle;
import com.sun.corba.se.spi.orbutil.threadpool.Work;
import org.apache.poi.ss.usermodel.*;

/**
 * @Auther: Anntly
 * @Date: 2020/3/29 17:24
 * @Description: 初始单元格样式
 */
public class StyleHandler {

    /**
     * @return com.excel.example.demo.entity.SelfCellStyle
     * @Description 初始化标题样式
     * @Date 2020/3/29 17:29
     * @Param [workbook, sourceStyle , targetCellStyle, targetStyle]
     **/
    public static SelfCellStyle initHeaderStyle(Workbook workbook, SelfCellStyle sourceStyle) {
        SelfCellStyle headerStyle = new SelfCellStyle();
        if (null == sourceStyle) {
            // 设置水平对齐的样式为居中对齐
            headerStyle.setAligh(HorizontalAlignment.CENTER);
            // 设置垂直对齐的样式为居中对齐
            headerStyle.setVertical(VerticalAlignment.CENTER);
            headerStyle.setFontName("宋体");
            headerStyle.setFontSize((short) 14);
            headerStyle.setItalic(false);
            headerStyle.setBold(true);
            headerStyle.setColors(IndexedColors.RED);
            headerStyle.setWidth(25);
            headerStyle.setHeight(30);
            headerStyle.buildCellStyle(workbook);
        } else {
            // 设置水平对齐的样式为居中对齐
            headerStyle.setAligh(sourceStyle.getAligh());
            // 设置垂直对齐的样式为居中对齐
            headerStyle.setVertical(sourceStyle.getVertical());
            headerStyle.setFontName(sourceStyle.getFontName());
            headerStyle.setFontSize(sourceStyle.getFontSize());
            headerStyle.setItalic(sourceStyle.isItalic());
            headerStyle.setBold(sourceStyle.isBold());
            headerStyle.setColors(sourceStyle.getColors());
            headerStyle.setWidth(sourceStyle.getWidth());
            headerStyle.setHeight(sourceStyle.getHeight());
            headerStyle.buildCellStyle(workbook);
        }
        return headerStyle;
    }

    /**
     * @return com.excel.example.demo.entity.SelfCellStyle
     * @Description 初始化普通单元格样式
     * @Date 2020/3/29 17:29
     * @Param [workbook, sourceStyle , targetCellStyle, targetStyle]
     **/
    public static SelfCellStyle initCellStyle(Workbook workbook, SelfCellStyle sourceStyle) {
        SelfCellStyle cellStyle = new SelfCellStyle();
        if (null == sourceStyle) {
            cellStyle.setAligh(HorizontalAlignment.LEFT);
            cellStyle.setVertical(VerticalAlignment.CENTER);
            cellStyle.setFontName("宋体");
            cellStyle.setFontSize((short) 10);
            cellStyle.setItalic(false);
            cellStyle.setBold(false);
            cellStyle.setColors(IndexedColors.BLACK);
            cellStyle.setWidth(25);
            cellStyle.setHeight(15);
            cellStyle.buildCellStyle(workbook);
        } else {
            // 设置水平对齐的样式为居中对齐
            cellStyle.setAligh(sourceStyle.getAligh());
            // 设置垂直对齐的样式为居中对齐
            cellStyle.setVertical(sourceStyle.getVertical());
            cellStyle.setFontName(sourceStyle.getFontName());
            cellStyle.setFontSize(sourceStyle.getFontSize());
            cellStyle.setItalic(sourceStyle.isItalic());
            cellStyle.setBold(sourceStyle.isBold());
            cellStyle.setColors(sourceStyle.getColors());
            cellStyle.setWidth(sourceStyle.getWidth());
            cellStyle.setHeight(sourceStyle.getHeight());
            cellStyle.buildCellStyle(workbook);
        }
        return cellStyle;
    }


    /**
     * 拷贝样式
     * 不同的workBook的CellStyle不能通用，需要单独创建
     *
     * @param sourceStyle
     * @param targetStyle
     * @param font
     */
    public static SelfCellStyle copyCellStyle(SelfCellStyle sourceStyle, Workbook sourceWorkBook, boolean isHead) {
        SelfCellStyle selfCellStyle;
        if (isHead) {
            selfCellStyle = initHeaderStyle(sourceWorkBook, sourceStyle);
        } else {
            selfCellStyle = initCellStyle(sourceWorkBook, sourceStyle);
        }
        return selfCellStyle;
    }


}