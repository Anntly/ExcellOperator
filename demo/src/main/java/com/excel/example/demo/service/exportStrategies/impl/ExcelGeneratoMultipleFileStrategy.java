package com.excel.example.demo.service.exportStrategies.impl;

import com.alibaba.fastjson.JSONObject;
import com.excel.example.demo.entity.CascadeDropdownData;
import com.excel.example.demo.entity.SelfCellStyle;
import com.excel.example.demo.enums.ExcelVersion;
import com.excel.example.demo.handlers.DropDownListHandler;
import com.excel.example.demo.handlers.StyleHandler;
import com.excel.example.demo.service.exportStrategies.AbstractExcelGeneratorStrategy;
import org.apache.poi.ss.formula.functions.T;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @Auther: Anntly
 * @Date: 2020/3/30 13:16
 * @Description: 多文件Excel生成策略
 */
public class ExcelGeneratoMultipleFileStrategy extends AbstractExcelGeneratorStrategy {

    private String fileName;
    // 隐藏的列
    private List<Integer> hiddenColumns;
    // 下拉框数据,true代表按照行插入；false代表按照列插入；integer代表在多少行/列插入，String[]为数据
    private Map<Boolean, Map<Integer, String[]>> dropdownList;
    // 级联下拉框
    private List<CascadeDropdownData> cascadeDropdownDataList;
    // 判断是JSONObject还是实体类
    private boolean dataType;

    public ExcelGeneratoMultipleFileStrategy(List dataList,String fileName, InputStream templateStream, String sheetName,
                                             Class clazz, String[] headers, ExcelVersion version,
                                             boolean needHeaderStyle, boolean needCellStyle,
                                             SelfCellStyle headStyle, SelfCellStyle cellStyle,
                                             Integer maxRow, Integer maxColumn,
                                             Map<String, DateTimeFormatter> dateFormatterPattern,
                                             Field[] declaredFields, List<Integer> hiddenColumns,
                                             Map<Boolean, Map<Integer, String[]>> dropdownList,
                                             List<CascadeDropdownData> cascadeDropdownDataList) {
        super(templateStream, sheetName, clazz, headers, version, needHeaderStyle, needCellStyle, headStyle, cellStyle, maxRow, maxColumn, dateFormatterPattern, declaredFields);
        this.dataList = dataList;
        this.fileName = fileName;
        this.hiddenColumns = hiddenColumns;
        this.dropdownList = dropdownList;
        this.cascadeDropdownDataList = cascadeDropdownDataList;
        if (dataList.get(0) instanceof JSONObject) {
            this.dataType = true;
        }
    }

    @Override
    public void generateExcel(OutputStream outputStream) throws Exception {
        throw new UnsupportedOperationException("不支持单文件导出");
    }

    @Override
    public void generateExcelMultipleFile(ZipOutputStream zop) throws Exception {
        super.checkHeaderIsRight();
        boolean needOthers = hiddenColumns != null || dropdownList != null || cascadeDropdownDataList != null;
        try {
            int excelCount = computeSheetOrWorkbookCount(dataList.size());
            DropDownListHandler dropDownListHandler = new DropDownListHandler(1, 0, this.dropdownList);
            for (int i = 0; i < excelCount; i++) {
                Workbook wb = super.initWorkBook();
                wb = isBigData(wb);
                if(i == 0){
                    super.initCellStyle(wb);
                }
                Sheet sheet = null;
                if (templateStream != null) {
                    sheet = wb.getSheetAt(0);
                } else {
                    sheet = wb.createSheet(sheetName);
                }
                // 创建标题行
                Row headerRow = sheet.createRow(0);
                // 不同workBook的样式要不同,每个单独的excel需要拷贝样式
                SelfCellStyle wbHeadCellStyle = null;
                if (needHeaderStyle) {
                    wbHeadCellStyle = StyleHandler.copyCellStyle(headStyle, wb, true);
                }
                super.initHeaderRow(sheet, headerRow, wbHeadCellStyle);
                // 填充单元格数据
                List<T> excelDataList = dataList.subList(0, Math.min(maxRow,dataList.size()));
                // 拷贝样式,不同workBook的样式不能通用
                SelfCellStyle wbCellStyle = null;
                if (needCellStyle) {
                    wbCellStyle = StyleHandler.copyCellStyle(this.cellStyle, wb, false);
                }
                int index = 1;
                for (Object obj : excelDataList) {
                    Row row = sheet.createRow(index);
                    index++;
                    super.fillRowData(p,row,obj,wbCellStyle,dataType);
                }
                if(needOthers){
                    super.handleOrhersMuliteFiles(excelDataList.size(),dropDownListHandler,cascadeDropdownDataList,hiddenColumns,wb,sheet);
                }
                excelDataList.clear();
                zop.putNextEntry(new ZipEntry(fileName + i + version.getSuffix()));
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                // 由于XSSFWorkbook的write()方法传去MemoryStream对象后，会自动关闭传入的参数，
                // 导致再次使用putNextEntry()方法是报错Stream closed
                // xlsx文件直接写入zop,循环到第二次的时候会导致流中断
                wb.write(bos);
                bos.writeTo(zop);
                close(wb);
                bos.close();
            }
        } finally {
            zop.flush();
        }
    }
}