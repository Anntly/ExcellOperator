package com.excel.example.demo.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.excel.example.demo.entity.Student;
import com.excel.example.demo.mapper.StudentMapper;
import com.excel.example.demo.service.StudentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Anntly
 * @since 2020-03-28
 */
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements StudentService {

    @Autowired
    private StudentMapper studentMapper;


    @Override
    public List<Student> queryAll() {
        return studentMapper.selectList(null);
    }

    @Override
    public List<JSONObject> queryAll(String tableName) {
        return studentMapper.selectOneTable(tableName);
    }

}
