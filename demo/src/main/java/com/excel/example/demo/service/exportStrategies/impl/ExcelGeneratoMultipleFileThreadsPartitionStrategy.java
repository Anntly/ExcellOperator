package com.excel.example.demo.service.exportStrategies.impl;

import com.excel.example.demo.entity.CascadeDropdownData;
import com.excel.example.demo.entity.SelfCellStyle;
import com.excel.example.demo.enums.ExcelVersion;
import com.excel.example.demo.handlers.DropDownListHandler;
import com.excel.example.demo.handlers.StyleHandler;
import com.excel.example.demo.service.DataPartition;
import com.excel.example.demo.service.exportStrategies.AbstractExcelGeneratorStrategy;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @Auther: 张文
 * @Date: 2020/3/30 14:22
 * @Description: 多线程导出多文件，数据库分区方式
 */
public class ExcelGeneratoMultipleFileThreadsPartitionStrategy extends AbstractExcelGeneratorStrategy {

    private String fileName;
    private DataPartition dataPartition;
    private Map partitionParam;
    // 隐藏的列
    private List<Integer> hiddenColumns;
    // 下拉框数据,true代表按照行插入；false代表按照列插入；integer代表在多少行/列插入，String[]为数据
    private Map<Boolean, Map<Integer, String[]>> dropdownList;
    // 级联下拉框
    private List<CascadeDropdownData> cascadeDropdownDataList;
    // 本机的CPU核心数量
    static final int nThreads = Runtime.getRuntime().availableProcessors();
    private boolean needOthers;

    public ExcelGeneratoMultipleFileThreadsPartitionStrategy(DataPartition dataPartition,Map partitionParam,String fileName,InputStream templateStream, String sheetName, Class clazz,
                                                             String[] headers, ExcelVersion version, boolean needHeaderStyle,
                                                             boolean needCellStyle, SelfCellStyle headStyle, SelfCellStyle cellStyle,
                                                             Integer maxRow, Integer maxColumn, Map<String, DateTimeFormatter> dateFormatterPattern,
                                                             Field[] declaredFields, List<Integer> hiddenColumns,
                                                             Map<Boolean, Map<Integer, String[]>> dropdownList,
                                                             List<CascadeDropdownData> cascadeDropdownDataList) {
        super(templateStream, sheetName, clazz, headers, version, needHeaderStyle, needCellStyle, headStyle, cellStyle, maxRow, maxColumn, dateFormatterPattern, declaredFields);
        this.dataPartition = dataPartition;
        this.partitionParam = partitionParam;
        this.fileName = fileName;
        this.hiddenColumns = hiddenColumns;
        this.dropdownList = dropdownList;
        this.cascadeDropdownDataList = cascadeDropdownDataList;
        if (dataPartition == null) {
            throw new UnsupportedOperationException("请传入分区实现类");
        }
        if (!ExcelVersion.V2007.equals(version)) {
            throw new UnsupportedOperationException("请使用xlsx格式");
        }
    }

    @Override
    public void generateExcel(OutputStream outputStream) throws Exception {
        throw new UnsupportedOperationException("不支持单文件导出");
    }

    @Override
    public void generateExcelMultipleFile(ZipOutputStream zop) throws Exception {
        ExecutorService executorService = null;
        try {
            super.checkHeaderIsRight();
            needOthers = hiddenColumns != null || dropdownList != null || cascadeDropdownDataList != null;
            int dataCount = dataPartition.dataCount(partitionParam);
            if (dataCount <= 0) {
                throw new RuntimeException("未查询到需要导出的数据");
            }
            int excelCount = computeSheetOrWorkbookCount(dataCount);
            DropDownListHandler dropDownListHandler = new DropDownListHandler(1, 0, this.dropdownList);
            // 计算线程数量
            int threadNum = nThreads > excelCount ? excelCount + 1 : nThreads + 1;
            executorService = Executors.newFixedThreadPool(threadNum);
            CountDownLatch countDownLatch = new CountDownLatch(excelCount);
            for (int i = 0; i < excelCount; i++) {
                int pageSize = Math.min(maxRow, dataCount);
                dataCount = dataCount - (maxRow);
                SXSSFWorkbook wb = super.initSxssfWorkBook();
                if(i == 0){
                    if(i == 0){
                        super.initCellStyle(wb);
                    }
                }
                executorService.execute(new ExcelFileGenerator(dataPartition,partitionParam,i + 1, pageSize, fileName + i, wb, zop, countDownLatch));

            }
            countDownLatch.await();
        } finally {
            // 关闭线程池
            if(executorService != null){
                executorService.shutdown();
            }
            zop.flush();
            close(zop);
        }
    }

    // 只处理xlsx格式的文件
    class ExcelFileGenerator implements Runnable {

        private CountDownLatch countDownLatch;
        private List dataList;
        private String fileName;
        private SXSSFWorkbook workbook;
        private ZipOutputStream zop;
        private DataPartition dataPartition;
        private Map partitionParam;
        private int page;
        private int size;

        public ExcelFileGenerator(DataPartition dataPartition, Map partitionParam,
                                  int page, int size, String fileName, SXSSFWorkbook workbook,
                                  ZipOutputStream zop, CountDownLatch countDownLatch) {
            this.page = page;
            this.size = size;
            this.dataPartition = dataPartition;
            this.partitionParam = partitionParam;
            this.fileName = fileName;
            this.workbook = workbook;
            this.zop = zop;
            this.countDownLatch = countDownLatch;
        }

        @Override
        public void run() {
            ByteArrayOutputStream bos = null;
            SXSSFSheet sxssfSheet;
            if (null != templateStream) {
                sxssfSheet = workbook.getSheetAt(0);
            } else {
                sxssfSheet = workbook.createSheet(sheetName);
            }
            // 创建标题行
            Row headerRow = sxssfSheet.createRow(0);
            // 不同workBook的样式要不同
            SelfCellStyle wbHeadCellStyle = null;
            if (needHeaderStyle) {
                wbHeadCellStyle = StyleHandler.copyCellStyle(headStyle,workbook,true);
            }
            initHeaderRow(sxssfSheet, headerRow, wbHeadCellStyle);
            // 初始化数据
            this.dataList = dataPartition.getPageData(partitionParam, page, size);
            try {
                if (this.dataList != null && !this.dataList.isEmpty()) {
                    int index = 1;
                    // 单元格样式
                    SelfCellStyle wbCellStyle = null;
                    if (needCellStyle) {
                        wbCellStyle = StyleHandler.copyCellStyle(cellStyle,workbook,false);
                    }
                    for (Object obj : this.dataList) {
                        Row row = sxssfSheet.createRow(index);
                        index++;
                        fillRowData(p, row, obj, wbCellStyle, true);
                    }
                }
                if(needOthers){
                    DropDownListHandler dropDownListHandler = new DropDownListHandler(1, 0, dropdownList);
                    handleOrhersMuliteFiles(dataList.size(), dropDownListHandler, cascadeDropdownDataList, hiddenColumns, workbook, sxssfSheet);
                }
                // 写到压缩文件流需要加锁
                bos = new ByteArrayOutputStream();
                workbook.write(bos);
                synchronized (zop.getClass()) {
                    zop.putNextEntry(new ZipEntry(fileName + version.getSuffix()));
                    bos.writeTo(zop);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                countDownLatch.countDown();
                workbook.dispose();
                close(bos);
                dataList.clear();
            }
        }
    }
}