package com.excel.example.demo.enums;

public enum PositionEnum {
    ALIGN_GENERAL("general",0),
    ALIGN_LEFT("left",1),
    ALIGN_CENTER("center",2),
    ALIGN_RIGHT("right",3),
    VERTICAL_TOP("top",0),
    VERTICAL_CENTER("center",1),
    VERTICAL_BOTTOM("bottom",2),
    VERTICAL_JUSTIFY("justify",3),
    ;

    private String align;
    private Integer code;

    PositionEnum(String align, Integer code) {
        this.align = align;
        this.code = code;
    }

    public String getAlign() {
        return align;
    }

    public void setAlign(String align) {
        this.align = align;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
