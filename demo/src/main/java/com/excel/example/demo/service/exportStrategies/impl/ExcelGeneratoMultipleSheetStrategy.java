package com.excel.example.demo.service.exportStrategies.impl;

import com.alibaba.fastjson.JSONObject;
import com.excel.example.demo.entity.CascadeDropdownData;
import com.excel.example.demo.entity.SelfCellStyle;
import com.excel.example.demo.enums.ExcelVersion;
import com.excel.example.demo.handlers.DropDownListHandler;
import com.excel.example.demo.service.exportStrategies.AbstractExcelGeneratorStrategy;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

/**
 * @Auther: 张文
 * @Date: 2020/3/30 12:00
 * @Description: 多Sheet生成模式
 */
public class ExcelGeneratoMultipleSheetStrategy extends AbstractExcelGeneratorStrategy {

    // 隐藏的列
    private List<Integer> hiddenColumns;
    // 下拉框数据,true代表按照行插入；false代表按照列插入；integer代表在多少行/列插入，String[]为数据
    private Map<Boolean, Map<Integer, String[]>> dropdownList;
    // 级联下拉框
    private List<CascadeDropdownData> cascadeDropdownDataList;
    // 判断是JSONObject还是实体类
    private boolean dataType;


    public ExcelGeneratoMultipleSheetStrategy(List dataList, InputStream templateStream,
                                              String sheetName, Class clazz, String[] headers,
                                              ExcelVersion version, boolean needHeaderStyle,
                                              boolean needCellStyle, SelfCellStyle headStyle,
                                              SelfCellStyle cellStyle, Integer maxRow,
                                              Integer maxColumn, Map<String, DateTimeFormatter> dateFormatterPattern,
                                              Field[] declaredFields, List<Integer> hiddenColumns,
                                              Map<Boolean, Map<Integer, String[]>> dropdownList, List<CascadeDropdownData> cascadeDropdownDataList) {
        super(dataList, templateStream, sheetName, clazz, headers, version, needHeaderStyle, needCellStyle, headStyle, cellStyle, maxRow, maxColumn, dateFormatterPattern, declaredFields);
        this.hiddenColumns = hiddenColumns;
        this.dropdownList = dropdownList;
        this.cascadeDropdownDataList = cascadeDropdownDataList;
        if (dataList.get(0) instanceof JSONObject) {
            this.dataType = true;
        }
    }

    @Override
    public void generateExcel(OutputStream outputStream) {
        super.checkHeaderIsRight();
        Workbook workbook = null;
        try {
            workbook = super.initWorkBook();
            workbook = super.isBigData(workbook);
            super.initCellStyle(workbook);
            // 拆分sheet的数量
            int sheetCount = computeSheetOrWorkbookCount(dataList.size());
            // 保存每个sheet的数据量，用于创建下拉框的时候具体要关联的行数判断
            Map<Sheet, Integer> sheetNumMap = new HashMap<>();
            for (int i = 0; i < sheetCount; i++) {
                Sheet sheet;
                if (i == 0) {
                    // 如果第一个Sheet在模板已存在，直接获取，之后的sheet可以复制第一个Sheet的样式(暂未实现)
                    // 现在只保证第一个sheet的样式，之后都是新创建
                    if (templateStream != null) {
                        sheet = workbook.getSheetAt(0);
                    } else {
                        sheet = workbook.createSheet(sheetName);
                    }
                } else {
                    sheet = workbook.createSheet(sheetName + i);
                }
                // 计算每个Sheet中的行数
                if (i != sheetCount - 1) {
                    sheetNumMap.put(sheet, maxRow);
                } else {
                    // 最后一个sheet，行数为余数
                    sheetNumMap.put(sheet, dataList.size());
                }
                // 创建标题行
                Row headerRow = sheet.createRow(0);
                super.initHeaderRow(sheet, headerRow, headStyle);
                // 填充单元格数据
                // 计算下标，避免创建List占用额外的内存
                List sheetDataList = dataList.subList(0, Math.min(maxRow, dataList.size()));
                int index = 1;
                for (Object obj : sheetDataList) {
                    Row row = sheet.createRow(index);
                    index++;
                    super.fillRowData(p, row, obj, cellStyle, dataType);
                }
                // 清除内存
                sheetDataList.clear();
            }
            // 处理下拉框等
            if (hiddenColumns != null || dropdownList != null || cascadeDropdownDataList != null) {
                DropDownListHandler dropDownListHandler = new DropDownListHandler(1, 0, this.dropdownList);
                int rowNum;
                for (Map.Entry<Sheet, Integer> rows : sheetNumMap.entrySet()) {
                    Sheet key = rows.getKey();
                    // 如果没有数据需要填充，就默认关联最大行数的下拉框
                    rowNum = rows.getValue() == 0 ? maxRow : rows.getValue();
                    super.handleOrhersMuliteFiles(rowNum, dropDownListHandler, cascadeDropdownDataList, hiddenColumns, workbook, key);
                }
            }
            workbook.write(outputStream);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(workbook);
            close(outputStream);
        }
    }

    @Override
    public void generateExcelMultipleFile(ZipOutputStream zop) throws Exception {
        throw new UnsupportedOperationException("不支持多文件导出");
    }
}