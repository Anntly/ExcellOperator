package com.excel.example.demo.handlers;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * @Auther: Anntly
 * @Date: 2020/3/29 19:13
 * @Description: 处理流
 */
public class StreamHandler {


    /**
     * @Description 根据文件获取InputStream
     * @Date 2020/3/19 16:59
     * @Param [excelFile]
     **/
    public static Map<String, List<InputStream>> InitStreams(MultipartFile excelFile, String fileName) throws IOException {
        if (null == excelFile) {
            throw new RuntimeException("excel file not exist");
        }
        Map<String, List<InputStream>> inputStreams = new HashMap<>();
        List<InputStream> xlsList = new ArrayList<>();
        List<InputStream> xlsxList = new ArrayList<>();
        if (fileName.endsWith("zip")) {
            try {
                ZipInputStream zipInputStream = new ZipInputStream(excelFile.getInputStream(), Charset.forName("GBK"));
                BufferedInputStream bs = new BufferedInputStream(zipInputStream);
                ZipEntry zipEntry;
                byte[] bytes = null;
                String zipFileName = null;
                while ((zipEntry = zipInputStream.getNextEntry()) != null) { // 获取zip包中的每一个zip file entry
                    zipFileName = zipEntry.getName();
                    bytes = new byte[(int) zipEntry.getSize()];
                    bs.read(bytes, 0, (int) zipEntry.getSize());
                    InputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
                    if (zipFileName.endsWith("xls")) {
                        xlsList.add(byteArrayInputStream);
                    } else if (zipFileName.endsWith("xlsx")) {
                        xlsxList.add(byteArrayInputStream);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (fileName.endsWith("xls")) {
            xlsList.add(excelFile.getInputStream());
        } else if (fileName.endsWith("xlsx")) {
            xlsxList.add(excelFile.getInputStream());
        }
        if (!xlsList.isEmpty()) {
            inputStreams.put("xls", xlsList);
        }
        if (!xlsxList.isEmpty()) {
            inputStreams.put("xlsx", xlsxList);
        }
        return inputStreams;
    }

    /**
     * 获取输出流
     *
     * @param fileName
     * @param response
     * @return
     * @throws Exception
     */
    public static BufferedOutputStream getBufferedOutputStream(String fileName, HttpServletResponse response,String suffix) throws Exception {
        response.setContentType("application/vnd.ms-excel");
        response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8")  + suffix);
        return new BufferedOutputStream(response.getOutputStream());
    }

    /**
     * 获取输出流
     *
     * @param fileName
     * @param response
     * @return
     * @throws Exception
     */
    public static ServletOutputStream getServletOutputStream(String fileName, HttpServletResponse response, String suffix) throws Exception {
        response.setContentType("application/octet-stream; charset=utf-8");
        response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(fileName, "UTF-8") + suffix);
        return response.getOutputStream();
    }
}