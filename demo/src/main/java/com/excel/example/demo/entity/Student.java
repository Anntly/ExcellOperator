package com.excel.example.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.excel.example.demo.annotations.DateFormat;
import com.excel.example.demo.annotations.DropdownList;
import com.excel.example.demo.annotations.ExcelDesc;
import com.excel.example.demo.enums.DropdownListType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @Auther: Anntly
 * @Date: 2020/3/20 16:07
 * @Description:
 */
//@Data
//@EqualsAndHashCode(callSuper = false)
//@Accessors(chain = true)
@TableName("tb_student")
@ExcelDesc("学生")
public class Student implements Cloneable{
    /**
     * 序号，如果需要获取数据库自动生成ID，必须设置type=IdType.AUTO
     */
    @TableId(type= IdType.AUTO)
    @ExcelDesc("编号")
    private Integer id;
    @ExcelDesc("名字")
    private String name;
    @ExcelDesc("性别")
    private String sex;
    @DateFormat
    @ExcelDesc("生日")
    private Date birthday;
    @ExcelDesc("国家")
    private String country;
    @ExcelDesc("省")
    private String province;
    @ExcelDesc("市")
    private String city;
    @ExcelDesc("入学年级")
    @DropdownList(valueJson = "[\"2018\",\"2019\",\"2020\",\"2021\"]",
            type = DropdownListType.BY_COLUMN,index = 7)
    private String admissionYear;
    @ExcelDesc("年级")
    @DropdownList(valueJson = "[\"年级2018\",\"年级2019\",\"年级2020\",\"年级2021\"]",
            type = DropdownListType.BY_COLUMN,index = 8,isTopParent = true)
    private String grade;
    @ExcelDesc("班级")
    @DropdownList(valueJson = "{\"年级2018\":[\"1班\",\"2班\",\"3班\",\"4班\"],\"年级2019\":[\"1班\",\"2班\",\"3班\"],\"年级2020\":[\"1班\",\"2班\"],\"年级2021\":[\"1班\",\"2班\",\"3班\"]}",
            type = DropdownListType.BY_COLUMN,index = 9,parentIndex = 8)
    private String classRoom;

    @Override
    public Object clone() throws CloneNotSupportedException {
        Student student = (Student) super.clone();
        if(student.birthday != null){
            student.birthday = (Date) student.birthday.clone();
        }
        return student;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAdmissionYear() {
        return admissionYear;
    }

    public void setAdmissionYear(String admissionYear) {
        this.admissionYear = admissionYear;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }
}