# ExcellOperator

#### 介绍
自己写的一个Excel工具支持下拉框，多选下拉框，级联下拉框，隐藏列、多线程下载，解析等

#### 软件架构
软件架构说明

使用Java语言，基于Apache POI，进行了功能的封装；

导出支持支持单文件多个Sheet，多个文件单Sheet，自定义分页大小，多线程导出大量数据等功能；100w数据7列本机测试14秒左右的时间

导入采用生产者消费者模式，单个线程读取Excel(SAX)模式，多个线程批量提交数据，自定义批量提交的数据大小


#### 安装教程

1.  本人水平有限，可能存在部分bug和不尽人意的地方，就不打包了
2.  直接克隆本项目将代码copy出来就可以用了
3.  如果有什么疑问，或者使用有什么bug，欢迎联系本人QQ

#### 使用说明

代码大部分都有注释，本身是一个SpringBoot的Demo，可以直接运行。运行DemoApplication后访问http://localhost:8080/excel/page 即为文件上传页，http://localhost:8080/excel/export 为导出页面，后端代码在在ExcelController

[项目结构.md](./instructions/项目结构.md)
[导入.md](./instructions/导入.md)
[导出.md](./instructions/导出.md)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
